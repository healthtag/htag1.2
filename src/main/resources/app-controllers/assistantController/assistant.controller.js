﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('AssistantController', AssistantController);

    AssistantController.$inject = ['AssistantService', '$rootScope'];
    function AssistantController(AssistantService, $rootScope) {
        var asc = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                return true;
        }
        if (!logged)
            $location.path('/login');
        asc.assistant = null;
        asc.addAssistant = addAssistant;
        asc.doctor = null;
        initController();

        function initController() {
            asc.doctor = $rootScope.loggedDoctor;
            console.log(asc.doctor);
        }


        function addAssistant() {
            asc.dataLoading = true;
            asc.assistant.docId = asc.doctor;
            asc.assistant.assistantId = asc.doctor;
            AssistantService.Create(asc.assistant)
                .then(function (response) {

                    // if (response.success) {
                    FlashService.Success('assistant Added Successful', true);
                    // Reset the form model.
                    asc.assistant = {};
                        // Set back to pristine.
                    asc.forms.name ="";
                    asc.assistant.name="";
                // Since Angular 1.3, set back to untouched state.
                    // $location.path('/patient');
                    // } else {
                    //     FlashService.Error(response.message);
                    //     asc.dataLoading = false;
                    // }

                });
        }


    }

})();