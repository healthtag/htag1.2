﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('DoctorProfileController', DoctorProfileController);

    DoctorProfileController.$inject = ['$http','DoctorService' , '$cookies', 'ClinicService', 'UserService', '$rootScope', '$location'];
    function DoctorProfileController($http,DoctorService , $cookies, ClinicService, UserService, $rootScope, $location) {
        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                return true;
        }
        if (!logged)
            $location.path('/login');
        vm.Clinics = [];

        initController();

        function initController() {
            if ($rootScope.doctorVisit) {
                SetCredentials($rootScope.doctorVisit.email);
                vm.selectedDoctor = $rootScope.doctorVisit;
                loadAllClinics();

            }
            else {
               var DoctorProfile =  $cookies.getObject('DoctorProfile') || {};
                if (DoctorProfile) {
                    if (DoctorProfile.doctor) {
                        if (DoctorProfile.doctor.email) {
                            var email = DoctorProfile.doctor.email;
                            loadSelectedDoctor(email);
                        } else {
                            $location.url('/patient/doctorLog').replace();
                        }
                    } else {
                        $location.url('/patient/doctorLog').replace();
                    }
                } else {
                    $location.url('/patient/doctorLog').replace();
                }
            }
        }

        function loadSelectedDoctor(email) {

            DoctorService.GetById(email)
                .then(function (doctor) {
                    vm.selectedDoctor =doctor;
                    loadAllClinics();
                });
        }
        function loadAllClinics() {
            ClinicService.GetAllByDoctor(vm.selectedDoctor.email)
                .then(function (clinics) {
                    vm.Clinics = clinics;
                });
        }

        function SetCredentials(email) {
            ClearCredentials();

            $rootScope.DoctorProfile = {
                doctor: {
                    email: email,
                }
            };

            // set default auth header for http requests
            $http.defaults.headers.common['Doctor'] = 'Doctor ';

            // store user details in globals cookie that keeps user logged in for 1 week (or until they logout)
            var cookieExp = new Date();
            cookieExp.setDate(cookieExp.getDate() + 1);
            $cookies.putObject('DoctorProfile', $rootScope.DoctorProfile, {expires: cookieExp});
        }

        function ClearCredentials() {
            $rootScope.DoctorProfile = {};
            $cookies.remove('DoctorProfile');
            $http.defaults.headers.common.DoctorProfile = 'Doctor';
        }
    }
})();