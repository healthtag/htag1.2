(function () {
    'use strict';

    angular
        .module('app')
        .controller('LandingController', LandingController);

    LandingController.$inject = ['$location', 'SearchService' , '$rootScope', 'AuthenticationService', 'ClinicService'];

    function LandingController($location, SearchService, $rootScope, AuthenticationService, ClinicService) {

        var vm = this;
        vm.searchType;
        vm.type;
        vm.doctors = {};
        vm.phars = {};
        vm.laps = {};
        vm.rads = {};
        vm.search = search;
        vm.logOut = logOut;
        vm.addRequestDemo = addRequestDemo;
        vm.requestObject = {};
        vm.searchModel = {};
        vm.loggedIn = false;
        vm.pageType='#!/home';
        initController();

        function initController() {

            if ($rootScope.globals) {
                if ($rootScope.globals.currentUser){
                    vm.loggedIn = true;
                    var t  = $rootScope.globals.currentUser.type;
                    vm.pageType = '#!/'+ t
                }
            }
            vm.doctors = SearchService.doctors;
            vm.phars = SearchService.phars;
            vm.laps = SearchService.laps;
            vm.rads = SearchService.rads;
            vm.type = SearchService.type;
        }
        function logOut() {
            localStorage.clear();
            ClinicService.flag = 0;
            ClinicService.ClinicId = 0;
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
        }

        function search(page) {
            vm.type = vm.searchType;
            SearchService.type = vm.searchType;
            if (vm.searchType === 'Doctor') {
                SearchService.getDoctorData(vm.searchModel)
                    .then(function (doctor) {//"address", "phone", "doctorName", "email", "specialty", "subSpecialty"
                        vm.doctors = doctor;
                        SearchService.doctors = doctor;
                        if (page === 1)//from landing page
                            $location.path('/search');

                    });
            } else if (vm.searchType === 'Pharmacy') {
                SearchService.getPharsData(vm.searchModel)
                    .then(function (Pharmacies) {
                        vm.phars = Pharmacies;
                        SearchService.phars = Pharmacies;
                        if (page === 1)//from landing page
                            $location.path('/search');

                    });
            } else if (vm.searchType === 'Lap') {
                SearchService.getLapsData(vm.searchModel)
                    .then(function (lapsData) {
                        vm.laps = lapsData;
                        SearchService.laps = lapsData;
                        if (page === 1)//from landing page
                            $location.path('/search');

                    });
            } else if (vm.searchType === 'Radiation center') {
                SearchService.getRadsData(vm.searchModel)
                    .then(function (radsData) {
                        vm.rads = radsData;
                        SearchService.rads = radsData;
                        if (page === 1)//from landing page
                            $location.path('/search');

                    });
            } else {

            }

        }


        function addRequestDemo() {
            SearchService.addRequestDemo(vm.requestObject)
                .then(function (response) {
                    $('#myForm')[0].reset();
                    swal({
                        title: "Done!",
                        text: "Your Requst Demo Is Sent Successfully We will Contact you soon.",
                        icon: "success",
                        button: "OK!",
                    });
                });
        }

    }

})();