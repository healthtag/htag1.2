﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LapsController', LapsController);

    LapsController.$inject = ['LapsService', 'UserService', 'PatientService', '$rootScope', 'AuthenticationService', '$scope', '$location', 'examinationSheet'];

    function LapsController(LapsService, UserService, PatientService, $rootScope, AuthenticationService, $scope, $location, examinationSheetService) {
        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.laps = null;
        vm.user = null;
        vm.logOut = logOut;
        vm.saveChanges = saveChanges;
        vm.viewAnalysis = viewAnalysis;
        vm.GetPatientByCardNumber = GetPatientByCardNumber;
        vm.savePasswordChanges = savePasswordChanges;
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;
        vm.samePass = false;
        vm.changeProfile = false;
        vm.patient = {};
        vm.search = false;


        initController();

        function initController() {
            vm.changeProfile = false;
            loadCurrentLaps();
            getCurrentPatientList();

        }

        function viewAnalysis(patient) {
            if (patient) {
                //$cookieStore.put("ClinicId", vm.opendClinic.clinicId);
                localStorage.setItem("PatientId", patient.patientId);
                $location.url('/laps/analysisSheet').replace();
            }
        }

        function GetPatientByCardNumber() {
            PatientService.GetPatientByCardNumber(vm.CardNumber)
                .then(function (patient) {
                    if (patient !== null) {
                        vm.patient = patient;
                        vm.search = true;
                    } else {
                        vm.search = false;

                    }

                });
        }

        vm.confirmPassword = function () {
            vm.samePass = vm.newPassword === vm.confirmPass ? true : false;
        };
        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };


        function logOut() {
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
            $location.url('/login').replace();
        }

        function loadCurrentLaps() {

            LapsService.GetByEmail(vm.globaluserName)
                .then(function (laps) {
                    vm.laps = laps;
                    localStorage.setItem("lapId", vm.laps.lapId);
                    $scope.cropped.source = vm.laps.picture;
                    $('#img1').attr('src', vm.laps.picture);
                    $('#img2').attr('src', vm.laps.picture);
                    $('#img3').attr('src', vm.laps.picture);
                    $('#circle').attr('src', vm.laps.picture);
                    $('#imgcircle').attr('src', vm.laps.picture);

                });
        }


        function saveChanges() {
            vm.dataLoading = true;
            LapsService.Update(vm.laps)
                .then(function (response) {
                    if (response !== null) {
                        swal(
                            'Congratulation!',
                            'Changes Saved Successfully!',
                            'success'
                        );
                        vm.dataLoading = false;

                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                        vm.dataLoading = false;

                    }
                });
        }

        function savePasswordChanges() {
            vm.dataLoading2 = true;
            if (vm.oldPassword === vm.laps.userProfile.userPassword) {
                vm.laps.userProfile.userPassword = vm.newPassword;
                UserService.Update(vm.laps.userProfile)
                    .then(function (response) {
                        if (response !== null) {
                            swal(
                                'Congratulation!',
                                'Changes Saved Successfully!',
                                'success'
                            );
                            vm.dataLoading2 = false;

                        } else {
                            swal(
                                'Error!',
                                'Changes not Saved!',
                                'error'
                            );
                            vm.dataLoading2 = false;

                        }
                    });

            } else {

                swal(
                    'Error!',
                    'Old password is incorrect',
                    'error'
                );
                vm.dataLoading2 = false;
            }
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.laps.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updateLaps();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!"
            });
            $location.path('/laps').replace();
        }

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }
        function updateLaps() {
            LapsService.Update(vm.laps)
                .then(function () {
                    $location.path('/laps');

                });
        }


        function getCurrentPatientList() {
            if(localStorage.getItem("lapId")) {
                LapsService.getCurrentPatientList(getCurrentPatientListsuccess, localStorage.getItem("lapId"));
            }else{
                LapsService.GetByEmail(vm.globaluserName)
                    .then(function (laps) {
                        vm.laps = laps;
                        LapsService.getCurrentPatientList(getCurrentPatientListsuccess, vm.laps.lapId);
                        localStorage.setItem("lapId", vm.laps.lapId);
        });
            }}
        function getCurrentPatientListsuccess(resp) {
            vm.currentList = _.groupBy(resp.data, function (item) {
                if (item.follow.visitId.patientId) {
                    return item.follow.visitId.patientId.patientName
                }
            });
            console.log(vm.currentList);
        }

        vm.cancelTest = function (radio, patientIndex, followUpIndex) {
            examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'New', patientIndex, followUpIndex));
        };
        $('#ana').on('change', function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                vm.docName = input.files[0].name;
                vm.docSize = input.files[0].size;
                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        vm.attachmentValue = e.target.result;
                    });
                };
                reader.readAsDataURL(input.files[0])
            }
        });
        vm.saveRadio = function (radio, patientIndex, followUpIndex) {
            $rootScope.showLoader = true;
            var f = document.getElementById('ana').files[0],
                reader = new FileReader();
            // reader.readAsText(f);
            if (f) {
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        radio.attachment = e.target.result;
                        examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'Finished', patientIndex, followUpIndex));
                    });
                };
                reader.readAsDataURL(f);
            } else {
                examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'Finished', patientIndex, followUpIndex));
            }

        };
        function prePareRadiologyTosave(labTest, status, patientIndex, followUpIndex) {
            labTest.status = status;
            labTest.lapId = {}
            labTest.lapId.lapId = localStorage.getItem("lapId");
            return labTest;
        }

        function saveFollowUpRadioSuccess(resp) {
            $rootScope.showLoader = false;
            swal(
                'Congratlation!',
                'Examination Sheet and Treatment Plan Create Successfully',
                'success'
            ).then(function () {

                getCurrentPatientList();
            });
        }
        function addPatient() {
            vm.dataPatientLoading = true;
            vm.card = {};
            vm.patient.userProfile = {};
            vm.card.cardNumber = vm.patient.cardId.cardNumber;
            vm.card.cardPin = "";
            vm.patient.userProfile.loginName = vm.patient.mobileNumber;
            vm.patient.cardId = vm.card
            PatientService.addPatient(vm.patient)
                .then(function (response) {
                    if (response == '0') {
                        swal(
                            'Error!',
                            'Not a valid Card Data!',
                            'error'
                        );
                    }
                    else if (response == '2') {
                        swal(
                            'Error!',
                            'Error in Adding new patient!',
                            'error'
                        );
                    } else {
                        vm.patient = null;
                        $('#myModal3').modal('hide');
                        swal(
                            'Congratulation!',
                            'patient Created Successfully!',
                            'success'
                        ).then(function () {
                            localStorage.setItem("PatientId", response.patientId);
                            window.location.href = "/#!/examinationSheet";
                            // window.location.href = "/byba_ss_war_exploded/#!/examinationSheet";
                        });
                        vm.dataPatientLoading = false;
                    }
                });
        }

    }

})();