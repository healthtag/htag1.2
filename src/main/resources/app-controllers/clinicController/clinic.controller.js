﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ClinicController', ClinicController);

    ClinicController.$inject = ['ClinicService', '$rootScope', 'UserService'];
    function ClinicController(ClinicService, $rootScope, UserService) {
        var cli = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                return true;
        }
        if (!logged)
            $location.path('/login');
        cli.clinic = null;
        cli.addClinic = addClinic;
        cli.user = null;
        cli.allClinics = [];


        initController();

        function initController() {
            getUserData();
            loadAllClinics();
        }

        function loadAllClinics() {
            ClinicService.GetAllByDoctor($rootScope.globals.currentUser.email)
                .then(function (clinics) {
                    cli.allClinics = clinics;
                    console.log(clinics);
                });
        }

        function addClinic() {
            cli.dataLoading = true;
            cli.clinic.doctorId = cli.user.doctorId.userId;

            ClinicService.Create(cli.clinic)
                .then(function (response) {

                    // if (response.success) {
                    swal(
                        'Congratlation!',
                        'Clinic Added Successful!',
                        'success'
                    ).then(function() {
                        window.location.href = "/#!/login";
                        // window.location.href = "/byba_ss_war_exploded/#!/login";
                    });
                    $('#reopen_issue').modal('hide');

                    // Reset the form model.
                    // Since Angular 1.3, set back to untouched state.
                    // $location.path('/patient');
                    // } else {
                    //     FlashService.Error(response.message);
                    //     asc.dataLoading = false;
                    // }

                });
        }

        function getUserData() {

            UserService.GetByUsernames($rootScope.globals.currentUser.email)
                .then(function (user) {
                    cli.user = user;
                });
        }
    }

})();