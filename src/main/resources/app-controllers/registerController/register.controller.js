﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', 'PatientService', '$location', '$rootScope'];

    function RegisterController(UserService, PatientService, $location, $rootScope) {
        var vm = this;
        vm.userType = 'patient';
        vm.user = {};
        vm.user.userProfile = {};
        vm.user.userProfile.loginName = '';
        vm.user.userProfile.userPassword = '';
        vm.user.userProfile.mobileNumber = '';
        vm.register = register;
        vm.samePass = false;
        vm.emailAviable = false;
        vm.mobileAviable = true;
        vm.verificationNo = '';
        vm.verifyCode = '';
        vm.mobileValidationRegex = /^(010|011|012|015|013){1}([0-9]{8})$/;
        vm.showInvalidMobileNo = false;
        vm.showAddClinicDetails = false;
        vm.forgetPasswordFirstStep = true;
        vm.verfyCode = '';
        // $rootScope.logOut();
        vm.confirmPassword = function () {
            if (vm.confirmPass) {
                vm.samePass = vm.user.userProfile.userPassword === vm.confirmPass ? true : false;
            }

        };

        vm.checkMail = function () {
            if (vm.user.userProfile.loginName) {
                UserService.checkMail(vm.user.userProfile.loginName)
                    .then(function (response) {
                        if (response == 'true')
                            vm.emailAviable = true;
                        else
                            vm.emailAviable = false;
                    })
            }


        };
        vm.checkMobile = function () {
            if (vm.user.userProfile.mobileNumber) {
                if (!vm.mobileValidationRegex.test(vm.user.userProfile.mobileNumber)) {
                    vm.showInvalidMobileNo = true;
                    vm.mobileAviable = true;
                } else {
                    vm.showInvalidMobileNo = false;
                    UserService.checkMobile(vm.user.userProfile.mobileNumber)
                        .then(function (response) {
                            if (response == 'true')
                                vm.mobileAviable = true;
                            else
                                vm.mobileAviable = false;

                        })
                }
            } else {
                vm.mobileAviable = true;
                vm.showInvalidMobileNo = false;
            }
        };
        // vm.verify = function () {
        //     if (vm.userType != 'patient') {
        //         if (vm.verifyCode != '' && vm.verificationNo == vm.verifyCode) {
        //             $('#reopen_issue').modal('hide');
        //
        //             register();
        //         } else {
        //             swal("Try Again!", "Not The Same Code", "error");
        //         }
        //     } else {
        //         register();
        //     }
        // };
        function register() {
            vm.dataLoading = true;
            vm.user.userProfile.type = vm.userType;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (response) {
                        $('#reopen_issue').modal('hide');
                        $location.path('/login');
                        swal(
                            'Congratlation!',
                            'You Regisred Successfuly!',
                            'success'
                        ).then(function () {
                        });
                    } else {
                        swal(
                            'Error!',
                            'Usr Not Regisred!',
                            'error'
                        );
                    }
                });
        }

        vm.sendSMSToUser = function (changePassword) {
            if (vm.user.userProfile.mobileNumber) {
                if (changePassword) {
                    UserService.sendRestPasswordSms(vm.user.userProfile.mobileNumber);
                    swal('Done!', 'We Send SMS with the verification code  please continue next step!', 'success');
                    vm.forgetPasswordFirstStep = false;
                } else {
                    if (vm.userType === 'patient') {
                        UserService.sendSMSToUser(vm.user.userProfile.mobileNumber);
                    }
                }

            } else {
                alert('plz enter mobile no')
            }
        }
        vm.VerfyCode = function () {
            if (vm.user.userProfile.mobileNumber && vm.verificationNo) {
                if (vm.userType === 'patient') {
                    UserService.VerfyCode(vm.user.userProfile.mobileNumber, vm.verificationNo)
                        .then(function (response) {
                            if (response) {
                                register();
                            }
                            else {
                                swal(
                                    'Error!',
                                    'code is wrong try again please!',
                                    'error'
                                );
                            }
                        })
                } else {
                    if (vm.verificationNo === 'DC78GHOPA') {
                        register();
                    }
                }
            }
            else {
                // alert('plz enter mobile no,verifyCode')
                // should handle if user didn't enter any code
            }
        }
        function navigateToLoginPage() {
            $rootScope.logOut();
            $location.path('/login');
        }

        vm.showClinic = function () {
            if (vm.userType && vm.userType === 'doctor') {
                vm.showAddClinicDetails = true;
            } else {
                vm.showAddClinicDetails = false;
            }
        }

        vm.resetPassword = function () {
            var resetPasswordDto = {};
            resetPasswordDto.mobileNumber = vm.user.userProfile.mobileNumber;
            resetPasswordDto.code = vm.verfyCode
            resetPasswordDto.password = vm.user.userProfile.userPassword;
            UserService.resetPassword(resetPasswordDto).then(function (response) {
                if (response) {
                    navigateToLoginPage();
                    swal('Done!', 'password updated succesfully', 'success');
                } else {
                    swal('Error!', 'Changes not Saved!', 'error'
                    );
                }


            })
        }
    }

})();
