﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PatientHeadController', PatientHeadController);
    PatientHeadController.$inject = ['$location', 'PatientService', '$rootScope', '$scope', 'AuthenticationService'];
    function PatientHeadController($location, PatientService, $rootScope, $scope, AuthenticationService) {
        var vm = this;
        vm.globaluserName = $rootScope.globals.currentUser.email;

        vm.patient = null;
        vm.logOut = logOut;

        initController();



        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };


        function initController() {
            vm.changeProfile = false;
            if (PatientService.patient) {
                vm.patient = PatientService.patient;
                checkCardAssignAndLoadImages();
            } else {
                loadCurrentPatient();
            }
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.patient.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $('#file').on('change', function () {
            var input = this;
            vm.base64 = input;

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                vm.docName = input.files[0].name;
                vm.docSize = input.files[0].size;
                console.log(input.files[0].name);
                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {

                        vm.attachmentValue = e.target.result;

                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updatePatient();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            }).then(function() {
                window.location.reload();
            });
        };

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }


        function loadCurrentPatient() {
            PatientService.GetPatientById(vm.globaluserName)
                .then(function (patient) {
                    vm.patient = patient;
                    PatientService.patient = patient;
                    checkCardAssignAndLoadImages();
                });
        }


        function checkCardAssignAndLoadImages() {
            if (vm.patient.cardId != null)
                vm.cardAssigndFlag = vm.patient.cardId.isActive;
            else
                vm.cardAssigndFlag = false;
            if($scope.cropped)
                $scope.cropped.source = vm.patient.picture;
            $('#img1').attr('src', vm.patient.picture);
            $('#img2').attr('src', vm.patient.picture);
            $('#img3').attr('src', vm.patient.picture);
            $('#circle').attr('src', vm.patient.picture);
            $('#imgcircle').attr('src', vm.patient.picture);
        }
        function updatePatient() {
            PatientService.UpdatePatient(vm.patient)
                .then(function () {
                    vm.cardAssigndFlag = true;
                    $location.path('/patient');

                });
        }

        function logOut() {
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
            $location.url('/login').replace();
        }


    }

})();


