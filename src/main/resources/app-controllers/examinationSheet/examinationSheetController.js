/**
 * Created by Administrator on 2/24/2018.
 */

(function () {
    'use strict';

    var app = angular.module('app')
        .controller('examinationSheetController', examinationSheetController);

    examinationSheetController.$inject = ['PatientService', '$rootScope', 'examinationSheet', 'UserService',
        'ClinicService', 'RadiationsService', '$scope', '$location', '$routeParams', '$route'];
    function examinationSheetController(PatientService, $rootScope, examinationSheetService, UserService,
                                        ClinicService, RadiationsService, $scope, $location, $routeParams, $route) {

        var _this = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        _this.firstLetterGroupFn = function (item) {
            return item.name[0];
        };
        _this.panel = [4];
        _this.panel[2] = true;
        _this.doctorId = localStorage.getItem('doctorId');
        $scope.Math = window.Math;
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. examination sheet
        _this.showExaminationSheet = false;
        _this.showTretmentPlan = true;
        _this.visitDate = new Date();
        _this.createNewVisit = function () {
            _this.followUps = [];
            _this.examinationSheet = {};
            if (_this.examinationSheet.id) {
                delete _this.examinationSheet.id;
            }
            _this.examinationSheet.assement = '';
            _this.examinationSheet.blood = '';
            _this.examinationSheet.heartRate = '';
            _this.examinationSheet.height = '';
            _this.examinationSheet.notes = '';
            _this.examinationSheet.objective = '';
            _this.examinationSheet.subjective = '';
            _this.examinationSheet.respRate = '';
            _this.examinationSheet.tempreature = '';
            _this.examinationSheet.weight = '';
        };
        _this.createNewVisit();
        _this.partialSave = function () {
            console.log(_this.selectedRadiology)
        }
        _this.showPanel = function (index) {

            _.forEach(_this.panel, function (item, key) {
                _this.panel[key] = false;
                // item = false;
            })

            _this.panel[index] = true;
        };
        _this.calcBMI_BSA = function () {
            if (_this.examinationSheet.weight && _this.examinationSheet.height && !
                    isNaN(_this.examinationSheet.weight) && !isNaN(_this.examinationSheet.weight)) {
                _this.examinationSheet.bmi = Math.round((_this.examinationSheet.weight / (_this.examinationSheet.height * _this.examinationSheet.height)) * 100000) / 10;
                _this.examinationSheet.bsa = Math.round(Math.sqrt(_this.examinationSheet.weight * (_this.examinationSheet.height / 3600)) * 100) / 100;

            } else {
                _this.examinationSheet.bmi = '';
                _this.examinationSheet.bsa = '';
            }

        };
        _this.bloodPressureSystolic = ['30', '40', '50', '60', '70', '80', '90', '100', '110', '120', '130',
            '140', '150', '160', '170', '180', '190', '200', '210', '220', '230', '240', '250', '260', '270', '280', '290', '300'];
        _this.bloodPressureDiastolic = ['10', '20', '30', '40', '50', '60', '70', '80', '90', '100',
            '110', '120', '130', '140', '150', '160', '170', '180', '190', '200'];
        _this.pulseRates = ["normal", "tachycardia ", "bradycardia "];
        _this.pulseRhythmRates = ["regular ", "irregular irregularity", "regular irregularity"];

        //    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> treatment plan
        //---------------------------------------------common
        _this.addItem = function (item) {
            item.other = !item.other;
        };
        _this.deleteItem = function (items, index) {
            items.splice(index, 1);
        };
        _this.confirmSave = function () {
            swal({
                title: "Are you sure?",
                text: "are you sure you want to save ?",
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then(function (willDelete) {
                if (willDelete.value) {

                    _this.save();
                } else { // swal("Your imaginary file is safe!");
                }

            })
        };


        _this.save = function () {
            var followUp = {};
            followUp.drugsList = [];
            followUp.radiologyList = [];
            followUp.examinationSheetList = [_this.examinationSheet];
            followUp.analysisList = [];
            followUp.visitId = {};
            if (_this.followUps.length > 0 && _this.followUps[0] && _this.followUps[0].visitId.id) {
                followUp.visitId.id = _this.followUps[0].visitId.id
                // followUp.visitId.date = _this.visitDate ? visitDate : new Date()
            }
            followUp.date = _this.visitDate ? _this.visitDate : new Date();

            followUp.visitId.patientId = {};

            _.forEach(_this.requiredRadiology, function (item) {

                var obj = {
                    bodyPart: item.bodyPart,
                    direction: item.direction,
                    name: item.name,
                    notes: item.notes,
                    type: item.type,
                    status: 'New'
                };
                followUp.radiologyList.push(obj);
            });
            _.forEach(_this.analysisList, function (item) {
                var obj = {name: item.name, notes: item.notes, status: 'New'};
                followUp.analysisList.push(obj);
            });
            _.forEach(_this.treatmentList, function (item) {
                var obj = {
                    dose: item.dose,
                    duration: item.duration,
                    name: item.name,
                    frequency: item.frequency,
                    direction: item.direction,
                    notes: item.notes,
                };

                followUp.drugsList.push(obj);
            });

            // followUp.followId = 1;
            followUp.visitId.patientId.patientId = localStorage.getItem("PatientId");
            followUp.visitId.clinicId = {};
            followUp.visitId.clinicId.clinicId = localStorage.getItem("ClinicId");

            console.log(followUp);
            _this.myfollow = followUp;
            examinationSheetService.createFollow(followUp);
            swal(
                'Congratlation!',
                'Examination Sheet and Treatment Plan Create Successfully',
                'success'
            ).then(function () {
                location.reload();
            });

        };

        //      -------------------------------------radiology
        function getRadologylookups() {
            examinationSheetService.getLookupRadiology(getLookUpSuccess);
        }

        function getLookUpSuccess(resp) {
            _this.radiologyLookUps = resp.data;
            _this.radiologyLookUps.push({name: "other"});
            // return _this.radiologyLookUps;
        }

        function getRadiologyCheckBoxes() {
            // call web service
            // to be replaced
            return [
                {name: 'x-ray(Plain x-ray)', id: 27, value: false},
                {name: 'CT', id: 22, value: false},
                {name: 'MRI', id: 24, value: false},
                {name: 'Ultrasound', id: 29, value: false},
                {name: 'ECG', id: 32, value: false},
                {name: 'Echocardiogram', id: 33, value: false}
            ]
        }


        getRadologylookups();
        _this.requiredRadiology = [];
        _this.selectedRadiology = {};
        _this.selectedRadiology = {name: "-------please select-------", disabled: true, id: "--"};
        _this.selectedRadiology;

        _this.addRadiology = function (selectedRadilogy) {
            _this.requiredRadiology.push({
                name: selectedRadilogy.name,
                type: '',
                bodyPart: '',
                direction: '',
                notes: '',
                other: selectedRadilogy.id ? false : true
            });
            _this.selectedRadiology = {name: "-------please select-------", disabled: true, id: "--"};

        };
        _this.radiologyCheckBoxes = getRadiologyCheckBoxes();
        _this.addRadiologyFromCheckBox = function (checkBox, value) {
            if (value) {
                _this.addRadiology(checkBox);
            } else {
                _this.requiredRadiology.splice(_.findIndex(_this.requiredRadiology, function (item) {
                    // to be changed compare with id
                    return item.name == checkBox.name;
                }), 1);
            }
        };
        _this.deleteRadiologyItem = function (items, index, toDeleteItem) {
            items.splice(index, 1);
            if (_this.radiologyCheckBoxes) {
                var _index = _.findIndex(_this.radiologyCheckBoxes, function (item) {
                    // to be changed compare with id
                    return item.name == toDeleteItem.name;
                })
                if (_this.radiologyCheckBoxes[_index]) {
                    _this.radiologyCheckBoxes[_index].value = false;
                }
            }
        };
        _this.bodyParts = ['Skull', 'Brain', 'Neck', 'Chest', 'Abdomen', 'Pelvis', 'Spine whole',
            'Spine cervical', 'Spine thoracic', 'Spine lumbar', 'Spine sacral', 'right Shoulder', 'left Shoulder',
            'right Arm', 'left Arm', 'right Elbow', 'left Elbow', 'right Forearm', 'left Forearm', 'right Wrist', 'left Wrist',
            'right Hand', 'left Hand', 'right Hip', 'left Hip', 'right Femur', 'left Femur', 'right Knee', 'left Knee', 'right Leg',
            'left Leg', 'right Ankle', 'left Ankle', 'right Foot', 'left Foot',
            'Breast', 'Abdomen & Pelvis', 'Chest & Abdomen', 'Chest & Abdomen & Pelvis', 'Neck & Chest & Abdomen & Pelvis'];
        _this.radiologDirections = ['A/P', 'P/A', 'Lateral', 'Coronal', 'Axial'];


        //---------------------------------------------analysisList
        _this.analysisLookUps = [];
        _this.analysisCheckBoxes = [];
        _this.analysisList = [];
        _this.selectedAnalsis = {name: "-------please select-------", disabled: true, id: "--"};
        _this.deleteAnalysisItem = function (items, index, toDeleteItem) {
            items.splice(index, 1);
            if (_this.analysisCheckBoxes) {
                var _index = _.findIndex(_this.analysisCheckBoxes, function (item) {
                    // to be changed compare with id
                    return item.name == toDeleteItem.name;
                })
                if (_this.analysisCheckBoxes[_index]) {
                    _this.analysisCheckBoxes[_index].value = false;
                }
            }
        };
        function getAnalsislookups() {
            examinationSheetService.getLookupAnalysis(getanalysisLookUpSuccess);
        }

        function getanalysisLookUpSuccess(resp) {
            _this.analysisLookUps = resp.data
            _this.analysisLookUps.push({name: "other"});
        }

        function getAnalsisCheckBoxes() {
            // call web service
            // to be replaced
            return [
                {name: 'CBC', id: 27021, value: false},
                {name: 'Urine Analysis', id: 28234, value: false},
                {name: 'Blood Sugar', id: 26952, value: false},
                {name: 'Lipid Profile', id: 27674, value: false},
                {name: 'Liver Enzymes', id: 28326, value: false},
                {name: 'Stool examination', id: 28327, value: false},
                {name: 'B-HCG', id: 28328, value: false}
            ]
        }


        getAnalsislookups();
        _this.analysisCheckBoxes = getAnalsisCheckBoxes();
        _this.AddAnalysis = function (selectedAnalsis) {
            _this.analysisList.push({
                name: selectedAnalsis.name,
                notes: '',
                other: selectedAnalsis.id ? false : true
            })
            _this.selectedAnalsis = {name: "-------please select-------", disabled: true, id: "--"};
        }


        _this.addAnalysisFromCheckBox = function (checkBox, value) {
            if (value) {
                _this.AddAnalysis(checkBox);
            } else {
                _this.analysisList.splice(_.findIndex(_this.analysisList, function (item) {
                    // to be changed compare with id
                    return item.name == checkBox.name;
                }), 1);
            }
        }

        //----------------------------------------------------------treatment
        _this.treatmentLookUp = [];
        _this.treatmentList = [];
        _this.selectedTretment = {name: "-------please select-------", disabled: true, id: "--"};

        function getTretmentLookups() {
            examinationSheetService.getLookupDrugs(getTretmentLookupsSuccess);
        }

        function getTretmentLookupsSuccess(resp) {
            _this.treatmentLookUp = resp.data
            _this.treatmentLookUp.push({name: "other"});
        }

        getTretmentLookups();

        _this.addTretment = function (selected) {
            _this.treatmentList.push({
                name: selected.name,
                dose: '',
                duration: '',
                notes: '',
                other: selected.id ? false : true
            })
            _this.selectedTretment = {name: "-------please select-------", disabled: true, id: "--"};

        };
        _this.drugQuantities = [".1", "1/4", "1/3", '1/2', '1', '2', '3', '4', '5'];
        _this.drugFrequencies = ["1 time a day", "2 times a day", "3 times a day", "4 times a day", "5 times a day", "6 times a day",
            "Every 2 days", "Every 3 days", "Every 1 week", "Every 2 week", "Every 3 week", "Every 1 month"];
        _this.drugDurations = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "2 weeks", "3 weeks", "4 weeks", "Forever"];
        _this.drugDirections = ['Before meals', 'After meals', "With meals", "Morning", "Afternoon", 'Evening', 'Bedtime', 'When needed'];
        _this.autoFillNewVisitDrug = function (druglist) {
            _this.treatmentList = _this.treatmentList.concat(druglist);
        }

        //_________________________________________________________________________drugs
        _this.prescription = [];
        _this.prescription = [
            {name: 'bandol', dose: "1 tab", duration: "5 days", given: false},
            {name: 'flomox', dose: "1 tab", duration: "5 days", given: false},
            {name: 'curam', dose: "1 tab", duration: "5 days", given: false}
        ];
        _this.submit = function () {
            console.log(_this.prescription)
        };
        //_________________________________________________________________________tretment follow up list
        function getfollowUpDrugs() {
            examinationSheetService.getPatientFollowsDrugs(followUpDrugsSuccess, localStorage.getItem("PatientId"));
        }

        function followUpDrugsSuccess(resp) {
            _this.followUpDrugs = resp.data
        }


        getfollowUpDrugs();
        _this.saveFollowUpTreatment = function (followUp) {
            _this.followUpToSave = Object.assign({}, followUp);
            // _this.followUpToSave = Object.clone(followUp);
            delete  _this.followUpToSave.show;
            _.forEach(_this.followUpToSave.drugsList, function (item) {
                    item.pharId = {};
                    item.pharId.pharId = localStorage.getItem("PharmacyId");

                }
            );
            examinationSheetService.saveFollowUpTreatment(saveFollowUpDrugsSuccess, _this.followUpToSave);

        }
        function saveFollowUpDrugsSuccess(resp) {
            swal(
                'Congratlation!',
                'Examination Sheet and Treatment Plan Create Successfully',
                'success'
            ).then(function () {
                getfollowUpDrugs();
            });
        }

        //_________________________________________________________________________ lab follow up list
        function getfollowUpAnalysis() {
            if (localStorage.getItem("lapId"))
                examinationSheetService.getfollowUpAnalysis(followUpAnalysisSuccess, localStorage.getItem("PatientId"), localStorage.getItem("lapId"));
        }

        function followUpAnalysisSuccess(resp) {
            _this.followUpAnalysis = resp.data
        }


        getfollowUpAnalysis();
        _this.saveFollowUpAnaylsis = function (followUp) {
            _this.followUpToSave = Object.assign({}, followUp);
            // _this.followUpToSave = Object.clone(followUp);
            delete  _this.followUpToSave.show;

            console.log(_this.followUpToSave)

            examinationSheetService.saveFollowUpAnaylsis(saveFollowUpAnalsisSuccess, _this.followUpToSave);

        };
        function saveFollowUpAnalsisSuccess(resp) {
            swal(
                'Congratlation!',
                'Examination Sheet and Treatment Plan Create Successfully',
                'success'
            ).then(function () {
                //  location.reload();
                getfollowUpAnalysis();
            });
        }

        // $('#ana').on('change', function () {
        //     var input = this;
        //
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //         reader.onload = function (e) {
        //             // bind new Image to Component
        //             // $scope.$apply(function () {
        //
        //                 _this.attachmentValue = e.target.result;
        //
        //             // });
        //         }
        //         reader.readAsDataURL(input.files[0])
        //     }
        // });
        _this.addLabTestToProgress = function (radio, followUpIndex) {
            examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prepareLabTesToSave(radio, 'InProgress', followUpIndex));
        };
        _this.cancelLabTest = function (radio, followUpIndex) {
            examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prepareLabTesToSave(radio, 'New', followUpIndex));
        };
        _this.saveLabTest = function (radio, followUpIndex) {
            $rootScope.showLoader = true;
            _this.loading = true;
            var f = document.getElementById('ana').files[0],
                r = new FileReader();
            // r.readAsText(f);
            if (f) {
                r.onloadend = function (e) {
                    $scope.$apply(function () {
                        radio.attachment = e.target.result;
                        examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prepareLabTesToSave(radio, 'Finished', followUpIndex));

                    });
                };
                r.readAsDataURL(f);
            } else {
                examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prepareLabTesToSave(radio, 'Finished', followUpIndex));

            }
        };

        // _this.saveLabTest = function (radio, followUpIndex) {
        //     var f = document.getElementById('ana').files[0],
        //         reader = new FileReader();
        //     // reader.readAsText(f);
        //
        //     reader.onload = function (e) {
        //         radio.attachment = e.target.result;
        //         examinationSheetService.saveLabTest(saveFollowUpRadioSuccess, prepareLabTesToSave(radio, 'Finished', followUpIndex));
        //
        //     };
        //     reader.readAsDataURL(f);
        //
        // };
        function prepareLabTesToSave(labTest, status, followUpIndex) {
            labTest.status = status;
            labTest.follow = {};
            labTest.follow.followId = _this.followUpAnalysis[followUpIndex].followId;
            labTest.lapId = {};
            labTest.lapId.lapId = localStorage.getItem("lapId");
            return labTest;
        }

        //_________________________________________________________________________ radiology follow up list
        function getfollowUpRadio() {
            if (localStorage.getItem("radId"))
                examinationSheetService.getfollowUpRadio(followRadioSuccess, localStorage.getItem("PatientId"), localStorage.getItem("radId"));
        }

        function followRadioSuccess(resp) {
            _this.loading = false;
            _this.loading2 = false;

            _this.followUpRadio = resp.data
        }

        _this.convertDate = function convertDate(date) {
            var dateTimeString = moment(date).format("DD-MM-YYYY HH:mm:ss");
            return dateTimeString;
        };
        getfollowUpRadio();
        _this.saveFollowUpRadio = function (followUp) {
            _this.followUpToSave = Object.assign({}, followUp);
            // _this.followUpToSave = Object.clone(followUp);
            delete  _this.followUpToSave.show;
            _this.followUpToSave
            _.forEach(_this.followUpToSave.radiologyList, function (item) {
                    item.status = 'Finished';
                }
            );
            console.log(_this.followUpToSave)
            // examinationSheetService.saveFollowUpRadio(saveFollowUpRadioSuccess, _this.followUpToSave);

        };
        _this.addToProgress = function (radio, followUpIndex) {
            examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'InProgress', followUpIndex));
        };
        _this.cancelTest = function (radio, followUpIndex) {
            examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'New', followUpIndex));
        };
        _this.saveRadio = function (radio, followUpIndex) {
            _this.loading2 = true;
            $rootScope.showLoader = true;
            var f = document.getElementById('ra').files[0],
                r = new FileReader();
            if (f) {
                r.onloadend = function (e) {
                    $scope.$apply(function () {
                        radio.attachment = e.target.result;
                        examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'Finished', followUpIndex));
                    });
                };
                r.readAsDataURL(f);
            } else {
                examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'Finished', followUpIndex));

            }
        };

        function prePareRadiologyTosave(radio, status, followUpIndex) {
            radio.status = status;
            radio.follow = {}
            radio.follow.followId = _this.followUpRadio[followUpIndex].followId;
            radio.radioligyCenterId = {}
            radio.radioligyCenterId.radId = localStorage.getItem("radId");
            return radio;
        }

        function saveFollowUpRadioSuccess(resp) {
            $rootScope.showLoader = false;
            // swal(
            //     'Congratlation!',
            //     'Examination Sheet and Treatment Plan Create Successfully',
            //     'success'
            // ).then(function () {
            //  location.reload();
            _this.loading = false;
            _this.loading2 = false;

            getfollowUpRadio();
            // });
        }

        //_________________________________________________________________________Patient Info
        loadCurrentPatient();

        function loadCurrentPatient() {
            PatientService.GetPatientByIds(localStorage.getItem("PatientId"))
                .then(function (patient) {
                    _this.patient = patient;
                    loadAllAttacFiles();

                });
            getPatientLabName(localStorage.getItem("PatientId"))

        }

        function getPatientLabName(patientId) {
            PatientService.getPatientLabName(patientId).then(function (labName) {
                _this.patientLabName = labName;
            })
        }

        _this.emailAviable = false;
        _this.checkMail = function () {
            UserService.checkMail(_this.patient.email)
                .then(function (response) {
                    if (response == 'true')
                        _this.emailAviable = true;
                    else
                        _this.emailAviable = false;
                })

        };
        _this.saveChanges = function saveChanges() {
            _this.dataLoading = true;
            PatientService.UpdatePatient(_this.patient)
                .then(function (response) {
                    if (response !== null) {
                        $('#editPatientModal').modal('hide');
                        swal(
                            'Congratlation!',
                            'Changes Saved Successfuly!',
                            'success'
                        )
                        _this.dataLoading = false;

                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                        // _this.dataLoading = false;

                    }
                });
        }

        //_________________________________________________________________________phr
        _this.preview = function preview(picName, ext) {
            PatientService.getUploadFile(picName)
                .then(function (file) {
                    _this.imgPrev = file;
                    $('#imgPrev').modal('show');
                    // vm.typePrev = file.split(";")[0].split("/")[1];
                });
            // window.open('uploadedFiles/'+img, '_blank');
        };
        _this.previewAttached = null ;
        _this.openFile = function openFile(id, number, preview) {
            examinationSheetService.getAttachment(id, number)
                .then(function (res) {

                    if (res && res.attachment && preview) {
                        var content = res.attachment.split(',');
                        var contentType = content[0].split(':')[1]
                        _this.previewAttached = atob(content[1]);
                       // _this.previewAttached = res.attachment
                    } else if(res && res.attachment) {
                        var content = res.attachment.split(',');
                        var contentType = content[0].split(':')[1]
                        const byteCharacters = atob(content[1]);
                        var byteNumbers = new Array(byteCharacters.length);
                        for (var i = 0; i < byteCharacters.length; i++) {
                            byteNumbers[i] = byteCharacters.charCodeAt(i);
                        }
                        var byteArray = new Uint8Array(byteNumbers);
                        var filename = "attachment"
                        var linkElement = document.createElement('a');
                        try {
                            var blob = new Blob([byteArray], {type: contentType});
                            var url = window.URL.createObjectURL(blob);
                            linkElement.setAttribute('href', url);
                            linkElement.setAttribute("download", filename);
                            var clickEvent = new MouseEvent("click", {
                                "view": window,
                                "bubbles": true,
                                "cancelable": false
                            });
                            linkElement.dispatchEvent(clickEvent);
                        } catch (ex) {
                            console.log(ex);
                        }
                    }

                });
        };
        _this.openFile2 = function openFile2(value) {
            var res = value.split(',')[1];
            var dlnk = document.getElementById('dwnldLnk');
            dlnk.href = res;
            dlnk.click();
        };
        _this.attachment = null;
        _this.attachmentValue = null;

        $('#file').on('change', function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                _this.docName = input.files[0].name;
                _this.docSize = input.files[0].size;
                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        _this.attachmentValue = e.target.result;
                    });
                };
                reader.readAsDataURL(input.files[0])
            }
        });
        _this.add = function add(form) {
            _this.attachDataLoading = true;

            if (_this.docName === '') {
                alert('please select file');
                _this.attachDataLoading = false;
            } else if (_this.docSize >= 10000000) {
                swal("Cant Upload!", "this file size is more than 10 MB!", "error");
                _this.attachDataLoading = false;

            } else {
                var f = document.getElementById('file').files[0],
                    r = new FileReader();
                _this.docName = f.name;
                _this.docSize = f.size;

                r.onloadend = function (e) {
                    $scope.$apply(function () {
                        _this.attachment.attachment = e.target.result;
                        _this.attachment.by = 'doctor';

                        PatientService.uploadFiles(_this.patient.userProfile.userId, _this.docName, _this.attachment).then(function (response) {
                            if (response === 'true') {
                                _this.attachDataLoading = false;
                                swal({
                                    title: "Uploaded!",
                                    text: "Your Attachment Uploaded Successfully!",
                                    icon: "success",
                                }).then(function () {
                                    window.location.reload();
                                    _this.attachment.doctorName = null;
                                    _this.attachment.documentName = null;
                                    form.documentName.$dirty = false;
                                    form.doctorName.$dirty = false;
                                    _this.attachment.attachment = null;
                                    _this.attachmentValue = null;
                                    _this.attachment = null;
                                    f = null
                                    $('#file').val('');
                                });
                            } else {
                                _this.attachDataLoading = false;
                                swal("Cant Upload!", "this file Extention is not supported!", "error");

                            }
                        });
                    });
                };
                r.readAsDataURL(f)

            }

        };

        function loadAllAttacFiles() {
            PatientService.getUploadFiles(_this.patient.patientId)
                .then(function (allAttchFiles) {
                    _this.allAttchFiles = allAttchFiles;
                });
        };

        //_________________________________________________________________________followUps
        getfollowUps();
        function getfollowUps() {
            if (localStorage.getItem("ClinicId"))
                examinationSheetService.getfollowUps(getfollowUpsuccess, localStorage.getItem("PatientId"), localStorage.getItem("ClinicId"));
        }

        _this.currentPage = 0;
        _this.currentRadio = 0;
        _this.currentRadioIndex = 0;
        _this.currentLap = 0;
        _this.currentLapIndex = 0;
        _this.followRadioLapToShowOnPopUp = null;

        function getfollowUpsuccess(resp) {
            _this.followUps = resp.data;
        }

        _this.incrementPaginmation = function (isLap, incrementBy) {
            if (isLap) {
                _this.currentLap += incrementBy;
                _this.currentLapIndex++
            } else {
                _this.currentRadio += incrementBy;
                _this.currentRadioIndex++;
            }
        };
        _this.decrementPaginmation = function (isLap, incrementBy) {

            if (isLap) {
                _this.currentLap -= incrementBy;
                _this.currentLapIndex--
            } else {
                _this.currentRadio -= incrementBy;
                _this.currentRadioIndex--;
            }
        };
        _this.openRadioLapResultModal = function (followRadioLap) {
            _this.followRadioLapToShowOnPopUp = followRadioLap;
            $('#visitRadioLapDetails').modal('show');
        }

        // _________________________________________________________________________history

        _this.allVisits = [];
        function getPatientHistory() {
            examinationSheetService.getPatientHistory(getPatientHistorySuccess, localStorage.getItem("PatientId"));
        }

        getPatientHistory();
        function getPatientHistorySuccess(resp) {
            _this.allVisits = resp.data;
        }


        // _________________________________________________________________________analysis sheet page to add new analysis list from lab
        if (PatientService.openAddNewAnalysisOnLab) {
            $('#addAnalysis').modal('show');
            PatientService.openAddNewAnalysisOnLab = false;
        }
        _this.addNewAnalysisListFromLab = function () {
            if (_this.analysisList && _this.analysisList.length) {
                var LabSheetDto = {};
                LabSheetDto.followUp = {};
                LabSheetDto.followUp.drugsList = [];
                LabSheetDto.followUp.radiologyList = [];
                LabSheetDto.followUp.examinationSheetList = [];
                LabSheetDto.followUp.analysisList = [];
                LabSheetDto.followUp.visitId = {};
                _.forEach(_this.analysisList, function (item) {
                    var obj = {name: item.name, notes: item.notes, status: 'New'};
                    LabSheetDto.followUp.analysisList.push(obj);
                });
                LabSheetDto.lapId = localStorage.getItem("lapId");
                LabSheetDto.patientid = localStorage.getItem("PatientId");
                // console.log(LabSheetDto);
                examinationSheetService.saveExaminationSheetForLab(LabSheetDto).then(function (resp) {
                    if (resp) {
                        $('#addAnalysis').modal('hide');
                        // window.location.href = "#!/laps/currentTest";
                        swal(
                            'Congratlation!',
                            'Analysis List Created Successfully',
                            'success'
                        ).then(function () {
                            $location.path('/laps/currentTest');
                            $route.reload();
                        })
                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                    }
                })
            }
        };

        // _________________________________________________________________________end
    };
    app.filter('propsFilter', function () {
        return filterFun
    });

    function filterFun(items, letter) {
        if (!letter.name) {
            return items;
        }
        var filtered = [];
        var letterMatch = new RegExp(letter.name, 'i');
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (letterMatch.test(item.name)) {
                filtered.push(item);
            }
        }
        return filtered;
    };
    app.filter('startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            if (input)
                return input.slice(start);
            else return null
        }
    });


})();
