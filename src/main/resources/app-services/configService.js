(function () {
    'use strict';

    angular
        .module('app')
        .factory('ConfigService', ConfigService);

    ConfigService.$inject = [];
    function ConfigService() {
        var service = {};
        // service.addressUrl ='http://www.htag.health/bypass/';
         service.addressUrl ='http://127.0.0.1:8280/byba_ss_war_exploded/bypass/';

        return service;
    }
})();
