(function () {
    'use strict';

    angular
        .module('app')
        .factory('LapsService', LapsService);

    LapsService.$inject = ['$http', 'ConfigService'];
    function LapsService($http, ConfigService) {
        var service = {};

        service.GetByEmail = GetByEmail;
        service.Update = Update;
        service.getCurrentPatientList = getCurrentPatientList;
        var url = ConfigService.addressUrl;

        return service;


        function GetByEmail(email) {
            return $http.post(url + 'laps/getLapsProfile/' + email).then(handleSuccess, handleError('Error getting Laps Profile'));
        }

        function Update(laps) {
            return $http.put(url + 'laps/', laps).then(handleSuccess).catch(handleError);
        }

        function getCurrentPatientList(successCallBak, radId) {
            return $http.get(url + 'FollowUp/currentAnalysis/' + radId).then(successCallBak, handleError('Error getting analysis Profile'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }

})();
