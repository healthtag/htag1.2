﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http', 'ConfigService'];
    function UserService($http, ConfigService) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Login = Login;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.checkMail = checkMail;
        service.checkMobile = checkMobile;
        service.sendSMSToUser = sendSMSToUser;
        service.sendRestPasswordSms = sendRestPasswordSms;
        service.VerfyCode = VerfyCode;
        service.resetPassword = resetPassword;


        var url = ConfigService.addressUrl;

        return service;

        function GetAll() {
            return $http.get(url + 'userprofile/findAll').then(handleSuccess, handleError);
        }

        function GetById(id) {
            return $http.get(url + 'api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }

        function Login(user) {
            var c = $http.post(url + 'userprofile/login/', user).then(handleSuccess, handleError('Error getting user by username'));
            console.log(c);
            return c;
        }

        function checkMail(mail) {
            return $http.get(url + 'userprofile/checkMail/' + mail).then(handleSuccess).catch(handleError);
        }

        function checkMobile(mobile) {
            return $http.get(url + 'userprofile/checkMobile/' + mobile).then(handleSuccess, handleError('Error checkMobile'));
        }

        function Create(user) {
            return $http.post(url + 'userprofile/', user).then(handleSuccess).catch(handleError);
        }

        function Update(user) {
            return $http.put(url + 'userprofile/', user).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(user) {
            return $http.delete(url + 'userprofile/' + user.userID).then(handleSuccess, handleError('Error deleting user'));
        }

        function sendSMSToUser(mobileNumber) {
            return $http.post(url + 'userprofile/sendsms/'+mobileNumber).then(handleSuccess).catch(handleError);
        }

        function VerfyCode(mobileNumber, code) {
            return $http.post(url + 'userprofile/verifycode/' + mobileNumber + '/' + code).then(handleSuccess, handleError);
        }

        function sendRestPasswordSms(mobileNumber) {
            return $http.post(url + 'userprofile/sendpasswordcode/'+ mobileNumber).then(handleSuccess).catch(handleError);
        }

        function resetPassword(dto) {
            return $http.post(url + 'userprofile/updatePassword' , dto).then(handleSuccess).catch(handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            // return
            return function () {
                return {success: false, message: error};
            };
        }


    }

})();
