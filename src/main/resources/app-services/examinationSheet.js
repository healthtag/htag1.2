(function () {
    'use strict';

    angular
        .module('app')
        .factory('examinationSheet', examinationSheet);

    examinationSheet.$inject = ['$http', 'ConfigService'];
    function examinationSheet($http, ConfigService) {
        var service = {};

        service.getLookupDrugs = getLookupDrugs;
        service.getLookupRadiology = getLookupRadiology;
        service.getLookupAnalysis = getLookupAnalysis;
        service.createFollow = createFollow;
        service.getPatientFollowsDrugs = getPatientFollowsDrugs;
        service.saveFollowUpTreatment = saveFollowUpTreatment;
        service.getfollowUpAnalysis = getfollowUpAnalysis;
        service.saveFollowUpAnaylsis = saveFollowUpAnaylsis;
        service.getfollowUpRadio = getfollowUpRadio;
        service.saveFollowUpRadio = saveFollowUpRadio;
        service.saveRadio = saveRadio;
        service.saveLabTest = saveLabTest;
        service.getfollowUps = getfollowUps;
        service.getPatientHistory = getPatientHistory;
        service.getAttachment = getAttachment;
        service.saveExaminationSheetForLab = saveExaminationSheetForLab;

        var url = ConfigService.addressUrl;

        return service;

        function getAttachment(id, number) {
            return $http.get(url + 'FollowUp/getAttachment/' + id + '/' + number).then(handleSuccess, handleError('Error getting user by username'));
        }

        // function createFollow(followUp) {
        //     return $http.post(url + 'attachment/pdf').then(handleSuccess, handleError('Error getting Laps Profile'));
        // }

        function createFollow(followUp) {
            // console.log(followUp);
            return $http.post(url + 'FollowUp', followUp).then(handleSuccess, handleError('Error getting Laps Profile'));
        }

        function getLookupDrugs(getLookUpSuccess) {
            return $http.get(url + 'LookupDrugs').then(getLookUpSuccess, handleError('Error getting Laps Profile'));
        }

        function getLookupRadiology(getLookUpSuccess) {
            return $http.get(url + 'LookupRadiology').then(getLookUpSuccess, handleError('Error getting radiology Profile'));
        }

        function getLookupAnalysis(getLookUpSuccess) {
            return $http.get(url + 'LookupAnalysis').then(getLookUpSuccess, handleError('Error getting analysis Profile'));
        }

        function getPatientFollowsDrugs(getLookUpSuccess, patientId) {
            return $http.get(url + 'FollowUp/patientFollowsDrugs/' + patientId).then(getLookUpSuccess, handleError('Error getting analysis Profile'));
        }

        function saveFollowUpTreatment(getLookUpSuccess, followupTretment) {
            return $http.post(url + 'FollowUp/saveFollowUpTreatment', followupTretment).then(getLookUpSuccess, handleError('Error getting analysis Profile'));
        }

        function getfollowUpAnalysis(getLookUpSuccess, patientId, labId) {
            return $http.get(url + 'FollowUp/PatientAnalysis/' + patientId + '/' + labId).then(getLookUpSuccess, handleError('Error getting analysis Profile'));
        }

        function saveFollowUpAnaylsis(getLookUpSuccess, followupAnalysis) {
            return $http.post(url + 'FollowUp/saveFollowUpAnalysis', followupAnalysis).then(getLookUpSuccess, handleError('Error getting analysis Profile'));
        }

        function getfollowUpRadio(successCallBak, patientId, radioId) {
            return $http.get(url + 'FollowUp/PatientRadiologies/' + patientId + "/" + radioId).then(successCallBak, handleError('Error getting analysis Profile'));
        }

        function saveFollowUpRadio(successCallBak, followupAnalysis) {
            return $http.post(url + 'FollowUp/saveFollowUpRadiologies', followupAnalysis).then(successCallBak, handleError('Error getting analysis Profile'));
        }

        function saveRadio(successCallBak, radio) {
            return $http.put(url + 'Radiology/' + radio.id, radio).then(successCallBak, handleError('Error getting analysis Profile'));
        }

        function saveLabTest(successCallBak, lab) {
            return $http.put(url + 'Analysis/' + lab.id, lab).then(successCallBak, handleError('Error getting analysis Profile'));
        }


        function getfollowUps(successCallBak, patientId, clinicId) {
            return $http.get(url + 'FollowUp/patientLatestFollows/' + patientId + '/' + clinicId).then(successCallBak, handleError('Error getting analysis Profile'));
        }

        function getPatientHistory(successCallBak, patientId) {
            return $http.get(url + 'FollowUp/patientVisits/' + patientId).then(successCallBak, handleError('Error getting analysis Profile'));
        }

        function saveExaminationSheetForLab(followUp) {
            return $http.post(url + 'FollowUp/saveExaminationSheetForLab' , followUp).then(handleSuccess, handleError('Error getting Laps Profile'))
        }

        function handleSuccess(res,status, headers) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }

})();
