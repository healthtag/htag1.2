(function () {
    'use strict';

    angular
        .module('app')
        .factory('ClinicService', ClinicService);

    ClinicService.$inject = ['$http','ConfigService'];
    function ClinicService($http,ConfigService) {
        var service = {};

        service.Create = Create;
        service.Edit = Edit;
        service.Delete = Delete;
        service.GetAllByDoctor = GetAllByDoctor;
        service.getDoctorData = getDoctorData;
        service.getClinicById = getClinicById;
        service.flag = 0;
        service.ClinicId = 0;
        var url = ConfigService.addressUrl;

        return service;

        function getDoctorData() {
            return $http.get(url+'doctorSearch/').then(handleSuccess).catch(handleError);
        }

        function GetAllByDoctor(username) {
            return $http.get(url+'clinics/doctorclinics/' + username).then(handleSuccess, handleError('Error getting all users'));
        }

        function Create(clinic) {
            return $http.post(url+'clinics/', clinic).then(handleSuccess, handleError('Error creating Clinics'));
        }
        function Edit(clinic) {
            return $http.put(url+'clinics/'+clinic.clinicId, clinic).then(handleSuccess, handleError('Error creating Clinics'));
        }
        function Delete(clinicId) {
            return $http.delete(url+'clinics/'+clinicId).then(handleSuccess, handleError('Error creating Clinics'));
        }
        function getClinicById(id) {
            return $http.get(url+'clinics/'+id).then(handleSuccess, handleError('Error creating Clinics'));
        }


         function uploadFileToUrl(file, uploadUrl) {
                //FormData, object of key/value pair for form fields and values
                var fileFormData = new FormData();
                fileFormData.append('file', file);

                var deffered = $q.defer();
                $http.post(uploadUrl, fileFormData, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}

                }).success(function (response) {
                    deffered.resolve(response);

                }).error(function (response) {
                    deffered.reject(response);
                });

                return deffered.promise;
            }
        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
