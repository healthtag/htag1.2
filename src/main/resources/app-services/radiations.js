(function () {
    'use strict';

    angular
        .module('app')
        .factory('RadiationsService', RadiationsService);

    RadiationsService.$inject = ['$http','ConfigService'];
    function RadiationsService($http,ConfigService) {
        var service = {};

        service.GetByEmail = GetByEmail;
        service.getCurrentPatientList = getCurrentPatientList;
        service.Update = Update;
        service.radId = null;
        var url = ConfigService.addressUrl;

        return service;


        function GetByEmail(email) {
            return $http.post(url+'Radiations/getRadiationsProfile/' + email).then(handleSuccess, handleError('Error getting Laps Profile'));
        }

        function getCurrentPatientList(successCallBak, radId) {
            return $http.get(url + 'FollowUp/currentRadiologies/' + radId).then(successCallBak, handleError('Error getting analysis Profile'));
        }
        function Update(radiations) {
            return $http.put(url+'Radiations/',radiations).then(handleSuccess).catch(handleError);
        }


        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
