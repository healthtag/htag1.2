(function () {
    'use strict';

    angular
        .module('app')
        .factory('DoctorService', DoctorService);

    DoctorService.$inject = ['$http','ConfigService'];
    function DoctorService($http,ConfigService) {
        var service = {};

        service.GetAll = GetAll;
        service.GetByEmail = GetByEmail;
        service.checkName = checkName;
        service.Create = Create;
        service.Update = Update;
        service.UpdateDoctor = UpdateDoctor;
        service.getDoctorData = getDoctorData;
        service.getDoctorPatients = getDoctorPatients;
        service.Delete = Delete;
        var url = ConfigService.addressUrl;
        service.doctors ={};
        service.currentDoctor = {};

        return service;

        function GetAll() {
            return $http.get(url+'doctors/').then(handleSuccess).catch(handleError);

        }
        function getDoctorData(search) {
            return $http.post(url+'doctors/doctorSearch/',search).then(handleSuccess).catch(handleError);
        }

        function GetByEmail(email) {
            return $http.get(url+'doctors/getDoctorProfile/' + email).then(handleSuccess, handleError('Error getting user by username'));
        }
        function getDoctorPatients(id) {
            return $http.get(url+'patient/doctorPatients/' + id).then(handleSuccess, handleError('Error getting user by username'));
        }


        function Create(doctor) {
            return $http.post(url+'doctors/', doctor).then(handleSuccess, handleError('Error creating patient'));
        }
        function checkName(name) {
            return $http.get(url+'doctors/checkName/'+name).then(handleSuccess, handleError('Error getting user by username'));
        }

        function Update(user) {
            return $http.put(url+'userprofile/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }
        function UpdateDoctor(doctor) {
            return $http.put(url+'doctors/',doctor).then(handleSuccess).catch(handleError);
        }

        function Delete(user) {
            return $http.delete(url+'userprofile/' + user.userID).then(handleSuccess, handleError('Error deleting user'));
      }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
