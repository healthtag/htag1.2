(function () {
    'use strict';

    angular
        .module('app')
        .factory('PatientService', PatientService);

    PatientService.$inject = ['$http', 'ConfigService'];
    function PatientService($http, ConfigService) {
        var service = {};

        service.GetAll = GetAll;
        service.GetPatientById = GetById;
        service.GetPatientByIds = GetByIds;
        service.Create = Create;
        service.addPatient = addPatient;
        service.checkCard = checkCard;
        service.UpdatePatient = Update;
        service.uploadFiles = uploadFiles;
        service.checkName = checkName;
        service.getUploadFiles = getUploadFiles;
        service.getUploadFile = getUploadFile;
        service.GetPatientByCardNumber = GetPatientByCardNumber;
        service.verifyCode = verifyCode;
        service.SendRandomPassword = SendRandomPassword;
        service.Delete = Delete;
        service.getPatientTimeline = getPatientTimeline;
        service.getPatientLabName = getPatientLabName;
        var url = ConfigService.addressUrl;
        service.PatientId;
        service.patient;
        service.allAttchFiles;
        service.getDrugListDetails = getDrugListDetails;
        service.getLabTestDetails = getLabTestDetails;
        service.getRadiationDetails  = getRadiationDetails ;

        return service;
        function SendRandomPassword(passsword, mobileNumber) {
            // var request = "https://smsmisr.com/api/webapi/?username=FwmsKet4&password=LKBOqki93R&language=1&sender=Health%20Tag&mobile="+mobileNumber+"&message=Welcome to Health Tag Your Temp Passsword  is : "+passsword+"&DelayUntil=2017-09-13-13-30";
            // console.log(request);
            // return $http.post(request).then(handleSuccess, handleError);
        }

        function GetAll() {
            return $http.get(url + 'patient/findAll').then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(userId) {
            return $http.post(url + 'patient/getPatientProfile/' + userId).then(handleSuccess, handleError('Error getting user by username'));
        }

        function GetByIds(userId) {
            return $http.post(url + 'patient/getPatientProfileById/' + userId).then(handleSuccess, handleError('Error getting user by username'));
        }

        function GetPatientByCardNumber(CardNumber) {
            return $http.post(url + 'patient/GetPatientByCardNumber/' + CardNumber).then(handleSuccess, handleError('Error getting user by username'));
        }

        function uploadFiles(userId, docName, attachment) {
            return $http.post(url + 'patient/uploadFile/' + userId + "/" + docName, attachment).then(handleSuccess, handleError('Error getting user by username'));
        }

        function verifyCode(mobileNumber) {
            return $http.post(url + 'patient/verify/' + mobileNumber).then(handleSuccess, handleError('Error getting user by username'));
        }

        function checkCard(card) {
            return $http.post(url + 'CardsLookup/checkCardData/', card).then(handleSuccess, handleError('Error getting user by username'));
        }

        function getUploadFiles(userName) {
            return $http.get(url + 'patient/findAllAttachments/' + userName).then(handleSuccess, handleError('Error getting user by username'));
        }

        function getUploadFile(fileName) {
            var c = $http.get(url + 'patient/findAttachment/' + fileName).then(handleSuccess, handleError('Error getting user by username'));
            console.log(c);
            return c;
        }

        function checkName(name) {
            return $http.get(url + 'patient/checkName/' + name).then(handleSuccess, handleError('Error getting user by username'));
        }

        // function GetByUsernames(username) {
        //     return $http.post(url+'userprofile/getUserProfile/' + username).then(handleSuccess, handleError('Error getting user by username'));
        // }

        function Create(patient) {
            return $http.post(url + 'patient/', patient).then(handleSuccess, handleError('Error creating patient'));
        }

        function addPatient(patient) {
            return $http.post(url + 'patient/addPatient', patient).then(handleSuccess, handleError('Error creating patient'));
        }

        function Update(patient) {
            return $http.put(url + 'patient/', patient).then(handleSuccess).catch(handleError);
        }

        function Delete(patient) {
            return $http.delete(url + 'patient/' + patient.idNo).then(handleSuccess, handleError('Error deleting user'));
        }

        function getPatientTimeline(patientId) {
            return $http.get(url + 'FollowUp/patientTimeline/' + patientId).then(handleSuccess, handleError('Error deleting user'));
        }

        function getDrugListDetails(timeLineID) {
            return $http.get(url + 'FollowUp/openDrugs/' + timeLineID).then(handleSuccess, handleError('Error deleting user'));
        }

        function getLabTestDetails(timeLineID) {
            return $http.get(url + 'FollowUp/openAnalysis/' + timeLineID).then(handleSuccess, handleError('Error deleting user'));
        }

        function getRadiationDetails(timeLineID) {
            return $http.get(url + 'FollowUp/openRadiology/' + timeLineID).then(handleSuccess, handleError('Error deleting user'));
        }


        function getPatientLabName(patientId) {
            return $http.post(url + 'patient/getLabNameByPatientId/' + patientId).then(handleSuccess, handleError('Error deleting user'));
        }
        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }

})();
