(function () {
    'use strict';

    angular
        .module('app')
        .factory('ValueService', ValueService);

    ValueService.$inject = ['$http','ConfigService','$cookies', '$rootScope'];
    function ValueService($http,ConfigService,$cookies, $rootScope) {
        var service = {};

        service.setClinic = setClinic;
        service.getClinic = getClinic;

        return service;

        function setClinic(clinic) {
            $rootScope.clinic = clinic;
            // set default auth header for http requests
            $http.defaults.headers.common['Clinics'] = 'Basic ' +  $rootScope.clinic;

            // store user details in globals cookie that keeps user logged in for 1 week (or until they logout)
            var cookieExp = new Date();
            cookieExp.setDate(cookieExp.getDate() + 7);
            $cookies.putObject('opendClinic',  $rootScope.clinic, {expires: cookieExp});
        }

        function ClearCredentials() {
            $rootScope.clinic = {};
            $cookies.remove('opendClinic');
            $http.defaults.headers.common.Clinics = 'Basic';
        }



        function getClinic() {
            return  $rootScope.clinic;
        }


    }

})();
