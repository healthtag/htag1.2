(function () {
    'use strict';

    angular
        .module('app')
        .factory('AssistantService', AssistantService);

    AssistantService.$inject = ['$http','ConfigService'];
    function AssistantService($http,ConfigService) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        // service.GetByUsernames = GetByUsernames;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        var url = ConfigService.addressUrl;

        return service;

        function GetAll() {
            return $http.get(url+'assistant/findAll').then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get(url+'/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }

        // function GetByUsernames(username) {
        //     return $http.post(url+'userprofile/getUserProfile/' + username).then(handleSuccess, handleError('Error getting user by username'));
        // }

        function Create(assistant) {
            return $http.post(url+'assistant/', assistant).then(handleSuccess, handleError('Error creating assistant'));
        }

        function Update(user) {
            return $http.put(url+'userprofile/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(user) {
            return $http.delete(url+'userprofile/' + user.userID).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
