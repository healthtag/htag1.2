(function () {
    'use strict';

    angular
        .module('app')
        .factory('SearchService', SearchService);

    SearchService.$inject = ['$http','ConfigService'];
    function SearchService($http,ConfigService) {
        var service = {};

        service.getDoctorData = getDoctorData;
        service.getPharsData = getPharsData;
        service.getRadsData = getRadsData;
        service.getLapsData = getLapsData;
        service.addRequestDemo = addRequestDemo;
        var url = ConfigService.addressUrl;
        service.doctors ={};
        service.phars ={};
        service.laps ={};
        service.rads ={};
        service.type;

        return service;


        function addRequestDemo(requestDemoObject) {
            return $http.post(url+'requestDemo/',requestDemoObject).then(handleSuccess).catch(handleError);
        }
        function getDoctorData(search) {
            return $http.post(url+'doctors/doctorSearch/',search).then(handleSuccess).catch(handleError);
        }
        function getPharsData(search) {
            return $http.post(url+'pharmacies/pharSearch/',search).then(handleSuccess).catch(handleError);
        }
        function getRadsData(search) {
            return $http.post(url+'Radiations/radSearch/',search).then(handleSuccess).catch(handleError);
        }
        function getLapsData(search) {
            return $http.post(url+'laps/labSearch/',search).then(handleSuccess).catch(handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
