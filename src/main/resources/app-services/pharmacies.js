(function () {
    'use strict';

    angular
        .module('app')
        .factory('PharmaciesService', PharmaciesService);

    PharmaciesService.$inject = ['$http','ConfigService'];
    function PharmaciesService($http,ConfigService) {
        var service = {};

        service.GetByEmail = GetByEmail;
        service.Update = Update;
        var url = ConfigService.addressUrl;

        return service;


        function GetByEmail(email) {
            return $http.post(url+'pharmacies/getPharmaciesProfile/' + email).then(handleSuccess, handleError('Error getting Pharmacies Profile'));
        }
        function Update(pharmacy) {
            return $http.put(url+'pharmacies/',pharmacy).then(handleSuccess).catch(handleError);
        }


        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
