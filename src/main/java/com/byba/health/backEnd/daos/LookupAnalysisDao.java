package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.LookupAnalysis;

import javax.persistence.EntityManager;

/**
 * @author IbrahimShedid
 */

public class LookupAnalysisDao extends AbstractDao<LookupAnalysis> {

    private EntityManager em;

    public LookupAnalysisDao(EntityManager em) {
        super(LookupAnalysis.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
