package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.RadiationCenters;
import com.byba.health.middleWare.dto.RadSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class RadiationsDao extends AbstractDao<RadiationCenters> {

    private EntityManager em;

    public RadiationsDao(EntityManager em) {
        super(RadiationCenters.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public RadiationCenters getRadiationsByEmail(String mail) {

        RadiationCenters radiationCenters;
        Query query = em.createNamedQuery("RadiationCenters.findByEmail");
        query.setParameter("email", mail);
        try {
            radiationCenters = (RadiationCenters) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return radiationCenters;
    }

    public List<RadSearchDto> getRadsSearchData(SearchDto searchDto) {
        List<RadSearchDto> lapSearchDto;
        String sql = "";
        if (searchDto.getGovernment() != null) {
            sql = "select r from  RadiationCenters r where  r.government = '" + searchDto.getGovernment() + "' ";
        } else {
            sql = "select r from  RadiationCenters r";
        }
        return (List<RadSearchDto>) getEntityManager().createQuery(sql).getResultList();

    }
}