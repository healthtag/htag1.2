package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.Appointments;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * @author IbrahimShedid
 */

public class AppointmentsDao extends AbstractDao<Appointments> {

    private EntityManager em;

    public AppointmentsDao(EntityManager em) {
        super(Appointments.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
