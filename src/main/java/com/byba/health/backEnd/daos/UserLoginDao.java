package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.UserLogin;

import javax.persistence.EntityManager;

/**
 * @author IbrahimShedid
 */

public class UserLoginDao extends AbstractDao<UserLogin> {

    private EntityManager em;

    public UserLoginDao(EntityManager em) {
        super(UserLogin.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

}
