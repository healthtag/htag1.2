package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.Cardslookup;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class CardsLookupDao extends AbstractDao<Cardslookup> {

    private EntityManager em;

    public CardsLookupDao(EntityManager em) {
        super(Cardslookup.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    public Cardslookup getCardByNumber(String number) {

        Cardslookup cardslookup;
        Query query = em.createNamedQuery("Cardslookup.findByCardnumber");
        query.setParameter("cardNumber", number);
        try {
            cardslookup = (Cardslookup) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return cardslookup;
    }
    public Cardslookup getCardByPin(String pin) {

        Cardslookup cardslookup;
        Query query = em.createNamedQuery("Cardslookup.findByPincode");
        query.setParameter("pincode", pin);
        try {
            cardslookup = (Cardslookup) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return cardslookup;
    }
    public Cardslookup getCardByPinAndNumber(Cardslookup cardslookups) {

        Cardslookup cardslookup;
        Query query = em.createNamedQuery("Cardslookup.findByPinAndNumber");
        query.setParameter("pincode", cardslookups.getPincode());
        query.setParameter("cardnumber", cardslookups.getCardnumber());
        try {
            cardslookup = (Cardslookup) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return cardslookup;
    }


}
