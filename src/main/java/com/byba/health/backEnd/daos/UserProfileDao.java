package com.byba.health.backEnd.daos;


import com.byba.health.backEnd.entities.UserProfile;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

/**
 * @author IbrahimShedid
 */

public class UserProfileDao extends AbstractDao<UserProfile> {

    private EntityManager em;

    public UserProfileDao(EntityManager em) {
        super(UserProfile.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public UserProfile getUserByLoginName(String loginName) {

        UserProfile userprofile;
        Query query = em.createNamedQuery("UserProfile.findByLoginName");
        query.setParameter("loginName", loginName);
        try {
            userprofile = (UserProfile) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return userprofile;
    }
    public UserProfile getUserByMobileNumber(String mobileNumber) {

        UserProfile userprofile;
        Query query = em.createNamedQuery("UserProfile.findByMobileNumber");
        query.setParameter("mobileNumber", mobileNumber);
        try {
            userprofile = (UserProfile) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return userprofile;
    }

}
