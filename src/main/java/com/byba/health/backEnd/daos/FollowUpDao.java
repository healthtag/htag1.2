package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Visits;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class FollowUpDao extends AbstractDao<FollowUp> {

    private EntityManager em;

    public FollowUpDao(EntityManager em) {
        super(FollowUp.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public List<FollowUp> getFollowUpByVisits(List<Visits> visits) throws Exception {
            if (visits.size() != 0)
                return getEntityManager().createNamedQuery("FollowUp.findByVisits").setParameter("visits", visits).getResultList();
            else
                return null;
    }
    public List<FollowUp> getFollowUpByVisit(Visits visit) throws Exception {
                return getEntityManager().createNamedQuery("FollowUp.findByVisit").setParameter("visit", visit).getResultList();
    }
}


