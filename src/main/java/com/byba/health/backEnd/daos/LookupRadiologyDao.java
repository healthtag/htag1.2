package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.LookupRadiology;

import javax.persistence.EntityManager;

/**
 * @author IbrahimShedid
 */

public class LookupRadiologyDao extends AbstractDao<LookupRadiology> {

    private EntityManager em;

    public LookupRadiologyDao(EntityManager em) {
        super(LookupRadiology.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
