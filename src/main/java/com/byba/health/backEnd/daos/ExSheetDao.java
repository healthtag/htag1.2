package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.ExaminationSheet;
import com.byba.health.backEnd.entities.FollowUp;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class ExSheetDao extends AbstractDao<ExaminationSheet> {

    private EntityManager em;

    public ExSheetDao(EntityManager em) {
        super(ExaminationSheet.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    public List<ExaminationSheet> getExaminationSheetByFollow(FollowUp followUp) throws Exception{
        return getEntityManager().createNamedQuery("ExaminationSheet.findByFollow").setParameter("follow", followUp).getResultList();
    }


}
