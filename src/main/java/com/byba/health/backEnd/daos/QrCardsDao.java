package com.byba.health.backEnd.daos;


import com.byba.health.backEnd.entities.QrCards;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class QrCardsDao extends AbstractDao<QrCards> {

    private EntityManager em;

    public QrCardsDao(EntityManager em) {
        super(QrCards.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public QrCards getQrCodeByCardNumber(String cardNumber) {

        QrCards qrCards;
        Query query = em.createNamedQuery("QrCards.findByCardNumber");
        query.setParameter("cardNumber", cardNumber);
        try {
            qrCards = (QrCards) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return qrCards;
    }
    public List<QrCards> getListQrCodeByCardNumber(String cardNumber) {

        List<QrCards> qrCards;
        Query query = em.createNamedQuery("QrCards.findByCardNumber");
        query.setParameter("cardNumber", cardNumber);
        try {
            qrCards = (List<QrCards>) query.getResultList();
        } catch (Exception e) {
            return null;
        }
        return qrCards;
    }
    public void deleteQrCodeByCardNumber(String cardNumber) {
        Query query = em.createNamedQuery("QrCards.deleteByCardNumber");
        query.setParameter("cardNumber", cardNumber);
    }


}
