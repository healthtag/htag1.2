/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.daos;

import javax.persistence.EntityManager;
import java.util.List;
/**
 * @author IbrahimShedid
 */
public abstract class AbstractDao<T> {
    private Class<T> entityClass;

    public AbstractDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected AbstractDao() {
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public T edit(T entity) {
        return getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        return getEntityManager().createQuery("Select t from " + entityClass.getSimpleName() + " t").getResultList();
    }

    public int count() {
        return ((Long) getEntityManager().createQuery("Select count(t) from " + entityClass.getSimpleName() + " t").getSingleResult()).intValue();
    }

}

