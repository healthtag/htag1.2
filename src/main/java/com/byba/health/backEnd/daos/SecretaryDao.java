package com.byba.health.backEnd.daos;




import com.byba.health.backEnd.entities.Secretary;

import javax.persistence.EntityManager;

/**
 * @author IbrahimShedid
 */

public class SecretaryDao extends AbstractDao<Secretary> {

    private EntityManager em;

    public SecretaryDao(EntityManager em) {
        super(Secretary.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
