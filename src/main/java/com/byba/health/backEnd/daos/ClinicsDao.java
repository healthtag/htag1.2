package com.byba.health.backEnd.daos;


import com.byba.health.backEnd.entities.Clinics;
import com.byba.health.backEnd.entities.Doctors;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.middleWare.dto.SearchDto;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class ClinicsDao extends AbstractDao<Clinics> {

    private EntityManager em;

    public ClinicsDao(EntityManager em) {
        super(Clinics.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public List<Clinics> getClinicsByDoctor(UserProfile doctorId) {
        return getEntityManager().createNamedQuery("Clinics.findBydoctorId").setParameter("doctorId", doctorId).getResultList();
    }

    public List<Doctors> getClinicsBySearchDto(SearchDto searchDto) {
        String sql = "select address,phone1,doctor_name,email,specialty,sub_specialty from " +
                "clinics inner join doctors on clinics.doctor_id=doctors.doctor_id " +
                "where clinics.areaLocation = " + searchDto.getArea() + " and clinics.governmentLocation LIKE " + "%searchDto.getGovernment%";
        return getEntityManager().createNativeQuery(sql).getResultList();

    }

}
