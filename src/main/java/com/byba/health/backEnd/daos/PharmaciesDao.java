package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.Pharmacies;
import com.byba.health.middleWare.dto.PharSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class PharmaciesDao extends AbstractDao<Pharmacies> {

    private EntityManager em;

    public PharmaciesDao(EntityManager em) {
        super(Pharmacies.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public Pharmacies getPharmaciesByEmail(String mail) {

        Pharmacies pharmacies;
        Query query = em.createNamedQuery("Pharmacies.findByEmail");
        query.setParameter("email", mail);
        try {
            pharmacies = (Pharmacies) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return pharmacies;
    }
    public List<PharSearchDto> getPharsSearch(SearchDto searchDto) {

        Pharmacies pharmacies;
        String sql ="";
        if(searchDto.getGovernment() != null)
        {
            sql = "select p from  Pharmacies p where  p.government = '"+searchDto.getGovernment()+"' ";
        }else{
            sql = "select p from  Pharmacies p";
        }
        return (List<PharSearchDto>) getEntityManager().createQuery(sql).getResultList();
    }

}
