package com.byba.health.backEnd.lookUps;

/**
 * Created by Abdelrahman on 9/25/2017.
 */
public enum radiologyStatus {
    New,
    InProgress,
    Finished
}
