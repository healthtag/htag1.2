package com.byba.health.backEnd.lookUps;

/**
 * Created by Abdelrahman on 9/25/2017.
 */
public enum UserTypes {
    Doctor,
    Patient,
    Assistant,
    Pharmacist
}
