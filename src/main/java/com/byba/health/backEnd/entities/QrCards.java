package com.byba.health.backEnd.entities;

/**
 * Created by Queue on 11/19/2017.
 */

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@Table(name = "qr_cards")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "QrCards.findAll", query = "SELECT q FROM QrCards q"),
        @NamedQuery(name = "QrCards.findByCardId", query = "SELECT q FROM QrCards q WHERE q.cardId = :cardId"),
        @NamedQuery(name = "QrCards.findByCardNumber", query = "SELECT q FROM QrCards q WHERE UPPER(q.cardNumber) LIKE UPPER(:cardNumber)"),
        @NamedQuery(name = "QrCards.deleteByCardNumber", query = "delete FROM QrCards q WHERE UPPER(q.cardNumber) LIKE UPPER(:cardNumber)"),
        @NamedQuery(name = "QrCards.findByCardPin", query = "SELECT q FROM QrCards q WHERE UPPER(q.cardPin) LIKE UPPER(:cardPin)"),
        @NamedQuery(name = "QrCards.findByIsActive", query = "SELECT q FROM QrCards q WHERE q.isActive = :isActive")})
public class QrCards implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "card_id")
    private Long cardId;
    @Column(name = "card_number")
    private String cardNumber;
    @Column(name = "card_pin")
    private String cardPin;
    @Column(name = "is_active")
    private Boolean isActive;
    @OneToMany(mappedBy = "cardId")
    private List<Patient> patientList;

    public QrCards() {
    }

    public QrCards(Long cardId) {
        this.cardId = cardId;
    }

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardPin() {
        return cardPin;
    }

    public void setCardPin(String cardPin) {
        this.cardPin = cardPin;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @XmlTransient
    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardId != null ? cardId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QrCards)) {
            return false;
        }
        QrCards other = (QrCards) object;
        if ((this.cardId == null && other.cardId != null) || (this.cardId != null && !this.cardId.equals(other.cardId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication10.QrCards[ cardId=" + cardId + " ]";
    }

}
