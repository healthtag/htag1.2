/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "pharmacies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pharmacies.findAll", query = "SELECT p FROM Pharmacies p"),
    @NamedQuery(name = "Pharmacies.findByPharId", query = "SELECT p FROM Pharmacies p WHERE p.pharId = :pharId"),
    @NamedQuery(name = "Pharmacies.findByPharName", query = "SELECT p FROM Pharmacies p WHERE p.pharName = :pharName"),
    @NamedQuery(name = "Pharmacies.findByEmail", query = "SELECT p FROM Pharmacies p WHERE UPPER(p.email) LIKE UPPER(:email)"),
    @NamedQuery(name = "Pharmacies.findByPassword", query = "SELECT p FROM Pharmacies p WHERE p.password = :password"),
    @NamedQuery(name = "Pharmacies.findByMobileNo", query = "SELECT p FROM Pharmacies p WHERE p.mobileNo = :mobileNo"),
    @NamedQuery(name = "Pharmacies.findByAddMobileNo", query = "SELECT p FROM Pharmacies p WHERE p.addMobileNo = :addMobileNo"),
    @NamedQuery(name = "Pharmacies.findByGovernment", query = "SELECT p FROM Pharmacies p WHERE p.government = :government"),
    @NamedQuery(name = "Pharmacies.findByArea", query = "SELECT p FROM Pharmacies p WHERE p.area = :area"),
    @NamedQuery(name = "Pharmacies.findByAddress", query = "SELECT p FROM Pharmacies p WHERE p.address = :address"),
    @NamedQuery(name = "Pharmacies.findByNationalId", query = "SELECT p FROM Pharmacies p WHERE p.nationalId = :nationalId"),
    @NamedQuery(name = "Pharmacies.findByLicense", query = "SELECT p FROM Pharmacies p WHERE p.license = :license")})
public class Pharmacies implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "phar_id")
    private Long pharId;
    @Column(name = "phar_name")
    private String pharName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "mobile_no")
    private String mobileNo;
    @Column(name = "add_mobile_no")
    private String addMobileNo;
    @Column(name = "government")
    private String government;
    @Column(name = "area")
    private String area;
    @Column(name = "address")
    private String address;
    @Column(name = "national_id")
    private String nationalId;
    @Column(name = "license")
    private String license;
    @Column(name = "picture")
    private String picture;
    @JoinColumn(name = "phar_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    public Pharmacies() {
    }

    public Pharmacies(Long pharId) {
        this.pharId = pharId;
    }

    public Long getPharId() {
        return pharId;
    }

    public void setPharId(Long pharId) {
        this.pharId = pharId;
    }

    public String getPharName() {
        return pharName;
    }

    public void setPharName(String pharName) {
        this.pharName = pharName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddMobileNo() {
        return addMobileNo;
    }

    public void setAddMobileNo(String addMobileNo) {
        this.addMobileNo = addMobileNo;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pharId != null ? pharId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pharmacies)) {
            return false;
        }
        Pharmacies other = (Pharmacies) object;
        if ((this.pharId == null && other.pharId != null) || (this.pharId != null && !this.pharId.equals(other.pharId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Pharmacies[ pharId=" + pharId + " ]";
    }
    
}
