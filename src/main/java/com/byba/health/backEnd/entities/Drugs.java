/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Abdelrahman
 */
@Entity
@Table(name = "drugs")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Drugs.findAll", query = "SELECT d FROM Drugs d"),
        @NamedQuery(name = "Drugs.findById", query = "SELECT d FROM Drugs d WHERE d.id = :id"),
        @NamedQuery(name = "Drugs.findByName", query = "SELECT d FROM Drugs d WHERE d.name = :name"),
        @NamedQuery(name = "Drugs.findByDose", query = "SELECT d FROM Drugs d WHERE d.dose = :dose"),
        @NamedQuery(name = "Drugs.findByDuration", query = "SELECT d FROM Drugs d WHERE d.duration = :duration"),
        @NamedQuery(name = "Drugs.findByResult", query = "SELECT d FROM Drugs d WHERE d.result = :result"),
        @NamedQuery(name = "Drugs.findByFollowUps", query = "SELECT d FROM Drugs d WHERE d.follow in (:followUps)"),
        @NamedQuery(name = "Drugs.findByFollowUp", query = "SELECT d FROM Drugs d WHERE d.follow = :followUp"),
        @NamedQuery(name = "Drugs.findByNotes", query = "SELECT d FROM Drugs d WHERE d.notes = :notes")})
public class Drugs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "dose")
    private String dose;
    @Column(name = "duration")
    private String duration;
    @Column(name = "notes")
    private String notes;
    @Column(name = "frequency")
    private String frequency;
    @Column(name = "direction")
    private String direction;
    @Column(name = "result")
    private boolean result;

    @JoinColumn(name = "follow_id", referencedColumnName = "follow_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private FollowUp follow;
    @JoinColumn(name = "phar_id", referencedColumnName = "phar_id")
    @ManyToOne
    private Pharmacies pharId;

    public Drugs() {
    }

    public Pharmacies getPharId() {
        return pharId;
    }

    public void setPharId(Pharmacies pharId) {
        this.pharId = pharId;
    }

    public Drugs(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public FollowUp getFollow() {
        return follow;
    }

    public void setFollow(FollowUp follow) {
        this.follow = follow;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drugs)) {
            return false;
        }
        Drugs other = (Drugs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Drugs[ id=" + id + " ]";
    }

}
