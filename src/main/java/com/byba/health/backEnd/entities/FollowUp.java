/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Abdelrahman
 */
@Entity
@Table(name = "follow_up")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "FollowUp.findAll", query = "SELECT f FROM FollowUp f"),
        @NamedQuery(name = "FollowUp.findByFollowId", query = "SELECT f FROM FollowUp f WHERE f.followId = :followId"),
        @NamedQuery(name = "FollowUp.findByDate", query = "SELECT f FROM FollowUp f WHERE f.date = :date"),
        @NamedQuery(name = "FollowUp.findByVisits", query = "SELECT f FROM FollowUp f WHERE f.visitId in (:visits) order by f.date desc "),
        @NamedQuery(name = "FollowUp.findByVisit", query = "SELECT f FROM FollowUp f WHERE f.visitId = :visit order by f.date desc "),
        @NamedQuery(name = "FollowUp.findByNotes", query = "SELECT f FROM FollowUp f WHERE f.notes = :notes")})
public class FollowUp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "follow_id")
    private Long followId;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "notes")
    private String notes;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "follow",fetch = FetchType.LAZY)
    private List<Drugs> drugsList;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "follow",fetch = FetchType.LAZY)
    private List<Radiology> radiologyList;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "follow",fetch = FetchType.LAZY)
    private List<Analysis> analysisList;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "follow",fetch = FetchType.LAZY)
    private List<ExaminationSheet> examinationSheetList;
    @ManyToOne
    @JoinColumn(name = "visit_id", referencedColumnName = "visit_id")
    private Visits visitId;

    public FollowUp() {
    }

    public FollowUp(Long followId) {
        this.followId = followId;
    }

    public Long getFollowId() {
        return followId;
    }

    public void setFollowId(Long followId) {
        this.followId = followId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

//    @XmlTransient
    public List<Drugs> getDrugsList() {
        return drugsList;
    }

    public void setDrugsList(List<Drugs> drugsList) {
        this.drugsList = drugsList;
    }

//    @XmlTransient
    public List<Radiology> getRadiologyList() {
        return radiologyList;
    }

    public void setRadiologyList(List<Radiology> radiologyList) {
        this.radiologyList = radiologyList;
    }

//    @XmlTransient
    public List<ExaminationSheet> getExaminationSheetList() {
        return examinationSheetList;
    }

    public void setExaminationSheetList(List<ExaminationSheet> examinationSheetList) {
        this.examinationSheetList = examinationSheetList;
    }

//    @XmlTransient
    public List<Analysis> getAnalysisList() {
        return analysisList;
    }

    public void setAnalysisList(List<Analysis> analysisList) {
        this.analysisList = analysisList;
    }

    public Visits getVisitId() {
        return visitId;
    }

    public void setVisitId(Visits visitId) {
        this.visitId = visitId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (followId != null ? followId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FollowUp)) {
            return false;
        }
        FollowUp other = (FollowUp) object;
        if ((this.followId == null && other.followId != null) || (this.followId != null && !this.followId.equals(other.followId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.FollowUp[ followId=" + followId + " ]";
    }

}
