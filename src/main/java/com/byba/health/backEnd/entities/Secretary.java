/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "secretary")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Secretary.findAll", query = "SELECT s FROM Secretary s")
        , @NamedQuery(name = "Secretary.findBySecId", query = "SELECT s FROM Secretary s WHERE s.secId = :secId")
        , @NamedQuery(name = "Secretary.findByName", query = "SELECT s FROM Secretary s WHERE s.name = :name")
        , @NamedQuery(name = "Secretary.findByMobile", query = "SELECT s FROM Secretary s WHERE s.mobile = :mobile")
        , @NamedQuery(name = "Secretary.findByEmail", query = "SELECT s FROM Secretary s WHERE s.email = :email")
        , @NamedQuery(name = "Secretary.findByNationalId", query = "SELECT s FROM Secretary s WHERE s.nationalId = :nationalId")})
public class Secretary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "sec_id")
    private Long secId;
    @Column(name = "name")
    private String name;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;
    @Column(name = "national_id")
    private Integer nationalId;
    @Lob
    @Column(name = "picture")
    private byte[] picture;
    @JoinColumn(name = "doctor_id", referencedColumnName = "user_id")
    @ManyToOne
    private UserProfile doctorId;
    @JoinColumn(name = "sec_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    public Secretary() {
    }

    public Secretary(Long secId) {
        this.secId = secId;
    }

    public Long getSecId() {
        return secId;
    }

    public void setSecId(Long secId) {
        this.secId = secId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNationalId() {
        return nationalId;
    }

    public void setNationalId(Integer nationalId) {
        this.nationalId = nationalId;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public UserProfile getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(UserProfile doctorId) {
        this.doctorId = doctorId;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (secId != null ? secId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Secretary)) {
            return false;
        }
        Secretary other = (Secretary) object;
        if ((this.secId == null && other.secId != null) || (this.secId != null && !this.secId.equals(other.secId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Secretary[ secId=" + secId + " ]";
    }

}
