/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "examination_sheet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExaminationSheet.findAll", query = "SELECT e FROM ExaminationSheet e"),
    @NamedQuery(name = "ExaminationSheet.findById", query = "SELECT e FROM ExaminationSheet e WHERE e.id = :id"),
    @NamedQuery(name = "ExaminationSheet.findByHeartRate", query = "SELECT e FROM ExaminationSheet e WHERE e.heartRate = :heartRate"),
    @NamedQuery(name = "ExaminationSheet.findByBlood", query = "SELECT e FROM ExaminationSheet e WHERE e.blood = :blood"),
    @NamedQuery(name = "ExaminationSheet.findByRespRate", query = "SELECT e FROM ExaminationSheet e WHERE e.respRate = :respRate"),
    @NamedQuery(name = "ExaminationSheet.findByTempreature", query = "SELECT e FROM ExaminationSheet e WHERE e.tempreature = :tempreature"),
    @NamedQuery(name = "ExaminationSheet.findByWeight", query = "SELECT e FROM ExaminationSheet e WHERE e.weight = :weight"),
    @NamedQuery(name = "ExaminationSheet.findByHeight", query = "SELECT e FROM ExaminationSheet e WHERE e.height = :height"),
    @NamedQuery(name = "ExaminationSheet.findByFollow", query = "SELECT e FROM ExaminationSheet e WHERE e.follow = :follow"),
    @NamedQuery(name = "ExaminationSheet.findBySubjective", query = "SELECT e FROM ExaminationSheet e WHERE e.subjective = :subjective"),
    @NamedQuery(name = "ExaminationSheet.findByObjective", query = "SELECT e FROM ExaminationSheet e WHERE e.objective = :objective"),
    @NamedQuery(name = "ExaminationSheet.findByAssement", query = "SELECT e FROM ExaminationSheet e WHERE e.assement = :assement"),
    @NamedQuery(name = "ExaminationSheet.findByNotes", query = "SELECT e FROM ExaminationSheet e WHERE e.notes = :notes")})
public class ExaminationSheet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "heart_rate")
    private String heartRate;
    @Column(name = "blood")
    private String blood;
    @Column(name = "resp_rate")
    private String respRate;
    @Column(name = "tempreature")
    private String tempreature;
    @Column(name = "weight")
    private String weight;
    @Column(name = "height")
    private String height;
    @Column(name = "subjective")
    private String subjective;
    @Column(name = "objective")
    private String objective;
    @Column(name = "assement")
    private String assement;
    @Column(name = "notes")
    private String notes;
    @Column(name = "bmi")
    private String bmi;
    @Column(name = "bsa")
    private String bsa;
    @Column(name = "rythm")
    private String rythm;
    @Column(name = "bloodpresure2")
    private String bloodpresure2;
    @Column(name = "symptoms")
    private String symptoms;
    @Column(name = "objective_hide")
    private boolean objectiveHide;
    @Column(name = "assement_hide")
    private boolean assementHide;
    @Column(name = "notes_hide")
    private boolean notesHide;
    @JoinColumn(name = "follow_id", referencedColumnName = "follow_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private FollowUp follow;

    public ExaminationSheet() {
    }

    public boolean getObjectiveHide() {
        return this.objectiveHide;
    }

    public void setObjectiveHide(boolean objectiveHide) {
        this.objectiveHide = objectiveHide;
    }

    public boolean getNotesHide() {
        return notesHide;
    }

    public void setNotesHide(boolean notesHide) {
        this.notesHide = notesHide;
    }

    public boolean getAssementHide() {
        return this.assementHide;
    }

    public void setAssementHide(boolean assementHide) {
        this.assementHide = assementHide;
    }

    public ExaminationSheet(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getRespRate() {
        return respRate;
    }

    public void setRespRate(String respRate) {
        this.respRate = respRate;
    }

    public String getTempreature() {
        return tempreature;
    }

    public void setTempreature(String tempreature) {
        this.tempreature = tempreature;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getSubjective() {
        return subjective;
    }

    public void setSubjective(String subjective) {
        this.subjective = subjective;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getAssement() {
        return assement;
    }

    public void setAssement(String assement) {
        this.assement = assement;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public FollowUp getFollow() {
        return follow;
    }

    public void setFollow(FollowUp follow) {
        this.follow = follow;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExaminationSheet)) {
            return false;
        }
        ExaminationSheet other = (ExaminationSheet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.ExaminationSheet[ id=" + id + " ]";
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getBsa() {
        return bsa;
    }

    public void setBsa(String bsa) {
        this.bsa = bsa;
    }

    public String getRythm() {
        return rythm;
    }

    public void setRythm(String rythm) {
        this.rythm = rythm;
    }

    public String getBloodpresure2() {
        return bloodpresure2;
    }

    public void setBloodpresure2(String bloodpresure2) {
        this.bloodpresure2 = bloodpresure2;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

}
