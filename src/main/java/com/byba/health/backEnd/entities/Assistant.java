/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "assistant")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Assistant.findAll", query = "SELECT a FROM Assistant a")
        , @NamedQuery(name = "Assistant.findByAssistantId", query = "SELECT a FROM Assistant a WHERE a.assistantId = :assistantId")
        , @NamedQuery(name = "Assistant.findByName", query = "SELECT a FROM Assistant a WHERE a.name = :name")
        , @NamedQuery(name = "Assistant.findByEmail", query = "SELECT a FROM Assistant a WHERE a.email = :email")
        , @NamedQuery(name = "Assistant.findByMobile", query = "SELECT a FROM Assistant a WHERE a.mobile = :mobile")
        , @NamedQuery(name = "Assistant.findByNationalId", query = "SELECT a FROM Assistant a WHERE a.nationalId = :nationalId")
        , @NamedQuery(name = "Assistant.findByGender", query = "SELECT a FROM Assistant a WHERE a.gender = :gender")
        , @NamedQuery(name = "Assistant.findBySpecialty", query = "SELECT a FROM Assistant a WHERE a.specialty = :specialty")
        , @NamedQuery(name = "Assistant.findBySubSpecialty", query = "SELECT a FROM Assistant a WHERE a.subSpecialty = :subSpecialty")
        , @NamedQuery(name = "Assistant.findByDegree", query = "SELECT a FROM Assistant a WHERE a.degree = :degree")
        , @NamedQuery(name = "Assistant.findBySyndicateNumber", query = "SELECT a FROM Assistant a WHERE a.syndicateNumber = :syndicateNumber")
        , @NamedQuery(name = "Assistant.findByLicenseNumber", query = "SELECT a FROM Assistant a WHERE a.licenseNumber = :licenseNumber")})
public class Assistant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "assistant_id")
    private Long assistantId;
    @Column(name = "name")
    private String name;
    @Column(name = "email")
    private String email;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "national_id")
    private Integer nationalId;
    @Column(name = "gender")
    private String gender;
    @Column(name = "specialty")
    private String specialty;
    @Column(name = "sub_specialty")
    private String subSpecialty;
    @Column(name = "degree")
    private String degree;
    @Column(name = "syndicate_number")
    private String syndicateNumber;
    @Column(name = "license_number")
    private String licenseNumber;
    @Lob
    @Column(name = "picture")
    private byte[] picture;
    @JoinColumn(name = "assistant_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;
    @JoinColumn(name = "doc_id", referencedColumnName = "user_id")
    @ManyToOne
    private UserProfile docId;

    public Assistant() {
    }

    public Assistant(Long assistantId) {
        this.assistantId = assistantId;
    }

    public Long getAssistantId() {
        return assistantId;
    }

    public void setAssistantId(Long assistantId) {
        this.assistantId = assistantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getNationalId() {
        return nationalId;
    }

    public void setNationalId(Integer nationalId) {
        this.nationalId = nationalId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getSubSpecialty() {
        return subSpecialty;
    }

    public void setSubSpecialty(String subSpecialty) {
        this.subSpecialty = subSpecialty;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getSyndicateNumber() {
        return syndicateNumber;
    }

    public void setSyndicateNumber(String syndicateNumber) {
        this.syndicateNumber = syndicateNumber;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public UserProfile getDocId() {
        return docId;
    }

    public void setDocId(UserProfile docId) {
        this.docId = docId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assistantId != null ? assistantId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assistant)) {
            return false;
        }
        Assistant other = (Assistant) object;
        if ((this.assistantId == null && other.assistantId != null) || (this.assistantId != null && !this.assistantId.equals(other.assistantId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Assistant[ assistantId=" + assistantId + " ]";
    }

}
