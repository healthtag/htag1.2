/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "clinics")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Clinics.findAll", query = "SELECT c FROM Clinics c")
        , @NamedQuery(name = "Clinics.findByClinicId", query = "SELECT c FROM Clinics c WHERE c.clinicId = :clinicId")
        , @NamedQuery(name = "Clinics.findByGovernmentLocation", query = "SELECT c FROM Clinics c WHERE c.governmentLocation = :governmentLocation")
        , @NamedQuery(name = "Clinics.findByAreaLocation", query = "SELECT c FROM Clinics c WHERE c.areaLocation = :areaLocation")
        , @NamedQuery(name = "Clinics.findByAddress", query = "SELECT c FROM Clinics c WHERE c.address = :address")
        , @NamedQuery(name = "Clinics.findBySearchDto", query = "SELECT c FROM Clinics c WHERE c.areaLocation = :areaLocation and c.governmentLocation = :governmentLocation")
        , @NamedQuery(name = "Clinics.findByPhone1", query = "SELECT c FROM Clinics c WHERE c.phone1 = :phone1")
        , @NamedQuery(name = "Clinics.findByPhone2", query = "SELECT c FROM Clinics c WHERE c.phone2 = :phone2")
        , @NamedQuery(name = "Clinics.findByPhone3", query = "SELECT c FROM Clinics c WHERE c.phone3 = :phone3")
        , @NamedQuery(name = "Clinics.findByMobile", query = "SELECT c FROM Clinics c WHERE c.mobile = :mobile")
        , @NamedQuery(name = "Clinics.findBydoctorId", query = "SELECT c FROM Clinics c WHERE c.doctorId = :doctorId")
        , @NamedQuery(name = "Clinics.findByExaminationFees", query = "SELECT c FROM Clinics c WHERE c.examinationFees = :examinationFees")})
public class Clinics implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "clinic_id")
    private Integer clinicId;
    @Column(name = "government_location")
    private String governmentLocation;
    @Column(name = "area_location")
    private String areaLocation;
    @Column(name = "address")
    private String address;
    @Column(name = "phone1")
    private String phone1;
    @Column(name = "phone2")
    private String phone2;
    @Column(name = "phone3")
    private String phone3;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "examination_fees")
    private Integer examinationFees;
    @JoinColumn(name = "appointment_id", referencedColumnName = "app_id")
    @ManyToOne
    private Appointments appointmentId;
    @JoinColumn(name = "assitant_id", referencedColumnName = "user_id")
    @ManyToOne
    private UserProfile assitantId;
    @JoinColumn(name = "doctor_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private UserProfile doctorId;
    @JoinColumn(name = "sec_id", referencedColumnName = "user_id")
    @ManyToOne
    private UserProfile secId;
    @OneToMany(mappedBy = "clinicId",fetch = FetchType.LAZY)
    private List<Visits> visitsList;

    public Clinics() {
    }

    public Clinics(Integer clinicId) {
        this.clinicId = clinicId;
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }

    public String getGovernmentLocation() {
        return governmentLocation;
    }

    public void setGovernmentLocation(String governmentLocation) {
        this.governmentLocation = governmentLocation;
    }

    public String getAreaLocation() {
        return areaLocation;
    }

    public void setAreaLocation(String areaLocation) {
        this.areaLocation = areaLocation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone3() {
        return phone3;
    }

    public void setPhone3(String phone3) {
        this.phone3 = phone3;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getExaminationFees() {
        return examinationFees;
    }

    public void setExaminationFees(Integer examinationFees) {
        this.examinationFees = examinationFees;
    }

    public Appointments getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Appointments appointmentId) {
        this.appointmentId = appointmentId;
    }

    public UserProfile getAssitantId() {
        return assitantId;
    }

    public void setAssitantId(UserProfile assitantId) {
        this.assitantId = assitantId;
    }

    public UserProfile getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(UserProfile doctorId) {
        this.doctorId = doctorId;
    }

    public UserProfile getSecId() {
        return secId;
    }

    public void setSecId(UserProfile secId) {
        this.secId = secId;
    }

    @XmlTransient
    public List<Visits> getVisitsList() {
        return visitsList;
    }

    public void setVisitsList(List<Visits> visitsList) {
        this.visitsList = visitsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clinicId != null ? clinicId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clinics)) {
            return false;
        }
        Clinics other = (Clinics) object;
        if ((this.clinicId == null && other.clinicId != null) || (this.clinicId != null && !this.clinicId.equals(other.clinicId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Clinics[ clinicId=" + clinicId + " ]";
    }

}
