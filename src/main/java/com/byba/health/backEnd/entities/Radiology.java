/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "radiology")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Radiology.findAll", query = "SELECT r FROM Radiology r"),
    @NamedQuery(name = "Radiology.findById", query = "SELECT r FROM Radiology r WHERE r.id = :id"),
    @NamedQuery(name = "Radiology.findByName", query = "SELECT r FROM Radiology r WHERE r.name = :name"),
    @NamedQuery(name = "Radiology.findByType", query = "SELECT r FROM Radiology r WHERE r.type = :type"),
    @NamedQuery(name = "Radiology.findByBodyPart", query = "SELECT r FROM Radiology r WHERE r.bodyPart = :bodyPart"),
    @NamedQuery(name = "Radiology.findByResult", query = "SELECT r FROM Radiology r WHERE r.result = :result"),
    @NamedQuery(name = "Radiology.findByDirection", query = "SELECT r FROM Radiology r WHERE r.direction = :direction"),
    @NamedQuery(name = "Radiology.findByFollow", query = "SELECT r FROM Radiology r WHERE r.follow = :follow"),
    @NamedQuery(name = "Radiology.findByFollowAndStatus", query = "SELECT r FROM Radiology r WHERE " +
            " r.follow = :follow and (r.status = 'New' or (r.status = 'InProgress' and r.radioligyCenterId = :radId)) order by r.follow.date desc"),
    @NamedQuery(name = "Radiology.findByCurrentTest", query = "SELECT r FROM Radiology r WHERE " +
            " (r.status = 'InProgress' and r.radioligyCenterId = :radId) order by r.follow.visitId.patientId.patientName "),
        @NamedQuery(name = "Radiology.findByNotes", query = "SELECT r FROM Radiology r WHERE r.notes = :notes")})
public class Radiology implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "type")
    private String type;
    @Column(name = "body_part")
    private String bodyPart;
    @Column(name = "direction")
    private String direction;
    @Column(name = "notes")
    private String notes;
    @Column(name = "result")
    private String result;
    @Column(name = "status")
    private String status;

    @JoinColumn(name = "follow_id", referencedColumnName = "follow_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private FollowUp follow;

    @JoinColumn(name = "radioligy_center_id", referencedColumnName = "rad_id")
    @ManyToOne
    private RadiationCenters radioligyCenterId;
    @Column(name = "attachment")
    private String attachment;

    public Radiology() {
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }


    public Radiology(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBodyPart() {
        return bodyPart;
    }

    public void setBodyPart(String bodyPart) {
        this.bodyPart = bodyPart;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FollowUp getFollow() {
        return follow;
    }
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setFollow(FollowUp follow) {
        this.follow = follow;
    }

    public RadiationCenters getRadioligyCenterId() {
        return radioligyCenterId;
    }

    public void setRadioligyCenterId(RadiationCenters radioligyCenterId) {
        this.radioligyCenterId = radioligyCenterId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Radiology)) {
            return false;
        }
        Radiology other = (Radiology) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Radiology[ id=" + id + " ]";
    }

}
