package com.byba.health.backEnd.entities;

/**
 * Created by Queue on 11/19/2017.
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "attachments")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Attachments.findAll", query = "SELECT a FROM Attachments a"),
        @NamedQuery(name = "Attachments.findById", query = "SELECT a FROM Attachments a WHERE a.id = :id"),
        @NamedQuery(name = "Attachments.findByUserId", query = "SELECT a FROM Attachments a WHERE a.userId = :UserId"),
        @NamedQuery(name = "Attachments.findByDocumentName", query = "SELECT a FROM Attachments a WHERE a.documentName = :documentName"),
        @NamedQuery(name = "Attachments.findByDocumentType", query = "SELECT a FROM Attachments a WHERE a.documentType = :documentType"),
        @NamedQuery(name = "Attachments.findByDoctorName", query = "SELECT a FROM Attachments a WHERE a.doctorName = :doctorName"),
        @NamedQuery(name = "Attachments.findByDate", query = "SELECT a FROM Attachments a WHERE a.date = :date"),
        @NamedQuery(name = "Attachments.findByUploadedDate", query = "SELECT a FROM Attachments a WHERE a.uploadedDate = :uploadedDate")})
public class Attachments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "document_name")
    private String documentName;
    @Column(name = "document_path_name")
    private String documentPathName;
    @Column(name = "document_type")
    private String documentType;
    @Column(name = "doctor_name")
    private String doctorName;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "uploaded_date")
    @Temporal(TemporalType.DATE)
    private Date uploadedDate;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private UserProfile userId;
    @Column(name = "attachment")
    private String attachment;
    @Column(name = "ext")
    private String ext;
    @Column(name = "by")
    private String by;

    public Attachments() {
    }

    public Attachments(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public UserProfile getUserId() {
        return userId;
    }

    public String getDocumentPathName() {
        return documentPathName;
    }

    public void setDocumentPathName(String documentPathName) {
        this.documentPathName = documentPathName;
    }

    public void setUserId(UserProfile userId) {
        this.userId = userId;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attachments)) {
            return false;
        }
        Attachments other = (Attachments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication10.Attachments[ id=" + id + " ]";
    }

}
