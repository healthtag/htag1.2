/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Abdelrahman
 */
@Entity
@Table(name = "analysis")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Analysis.findAll", query = "SELECT a FROM Analysis a"),
        @NamedQuery(name = "Analysis.findById", query = "SELECT a FROM Analysis a WHERE a.id = :id"),
        @NamedQuery(name = "Analysis.findByName", query = "SELECT a FROM Analysis a WHERE a.name = :name"),
        @NamedQuery(name = "Analysis.findByResult", query = "SELECT a FROM Analysis a WHERE a.result = :result"),
        @NamedQuery(name = "Analysis.findByFollow", query = "SELECT a FROM Analysis a WHERE a.follow = :follow"),
        @NamedQuery(name = "Analysis.findByFollowAndStatus", query = "SELECT a FROM Analysis a WHERE " +
                " a.follow = :follow and (a.status = 'New' or (a.status = 'InProgress' and a.lapId = :lapId)) order by a.follow.date desc"),
        @NamedQuery(name = "Analysis.findByCurrentTest", query = "SELECT a FROM Analysis a WHERE " +
                " (a.status = 'InProgress' and a.lapId = :lapId) order by a.follow.visitId.patientId.patientName "),
        @NamedQuery(name = "Analysis.getCountOfTestsNotFinished",
                query = "SELECT count(a) FROM Analysis a where a.follow.visitId.patientId = :patientId and a.status <> 'Finished' "),

        @NamedQuery(name = "Analysis.findByNotes", query = "SELECT a FROM Analysis a WHERE a.notes = :notes")})
public class Analysis implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "notes")
    private String notes;
    @Column(name = "result")
    private String result;
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "follow_id", referencedColumnName = "follow_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private FollowUp follow;
    @JoinColumn(name = "lap_id", referencedColumnName = "lap_id")
    @ManyToOne
    private Laps lapId;
    @Column(name = "attachment")
    private String attachment;

    public Analysis() {
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Analysis(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Laps getLapId() {
        return lapId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLapId(Laps lapId) {
        this.lapId = lapId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public FollowUp getFollow() {
        return follow;
    }

    public void setFollow(FollowUp follow) {
        this.follow = follow;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Analysis)) {
            return false;
        }
        Analysis other = (Analysis) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Analysis[ id=" + id + " ]";
    }

}
