/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "doctors")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Doctors.findAll", query = "SELECT d FROM Doctors d")
        , @NamedQuery(name = "Doctors.findByDoctorId", query = "SELECT d FROM Doctors d WHERE d.doctorId = :doctorId")
        , @NamedQuery(name = "Doctors.findByDoctorName", query = "SELECT d FROM Doctors d WHERE d.doctorName = :doctorName")
        , @NamedQuery(name = "Doctors.findByEmail", query = "SELECT d FROM Doctors d WHERE UPPER(d.email) LIKE UPPER(:email)")
        , @NamedQuery(name = "Doctors.findByGender", query = "SELECT d FROM Doctors d WHERE d.gender = :gender")
        , @NamedQuery(name = "Doctors.findBySpecialty", query = "SELECT d FROM Doctors d WHERE d.specialty = :specialty")
        , @NamedQuery(name = "Doctors.findBySubSpecialty", query = "SELECT d FROM Doctors d WHERE d.subSpecialty = :subSpecialty")
        , @NamedQuery(name = "Doctors.findByDegree", query = "SELECT d FROM Doctors d WHERE d.degree = :degree")
        , @NamedQuery(name = "Doctors.findByNationalId", query = "SELECT d FROM Doctors d WHERE d.nationalId = :nationalId")
        , @NamedQuery(name = "Doctors.findBySyndicateNumber", query = "SELECT d FROM Doctors d WHERE d.syndicateNumber = :syndicateNumber")
        , @NamedQuery(name = "Doctors.findByLicenseNumber", query = "SELECT d FROM Doctors d WHERE d.licenseNumber = :licenseNumber")
        , @NamedQuery(name = "Doctors.findByMobileNumber", query = "SELECT d FROM Doctors d WHERE d.mobileNumber = :mobileNumber")
        , @NamedQuery(name = "Doctors.findByArabicDoctorName", query = "SELECT d FROM Doctors d WHERE d.arabicDoctorName = :arabicDoctorName")
        , @NamedQuery(name = "Doctors.findByBriefProfile", query = "SELECT d FROM Doctors d WHERE d.briefProfile = :briefProfile")
        , @NamedQuery(name = "Doctors.findByBio", query = "SELECT d FROM Doctors d WHERE d.bio = :bio")
        , @NamedQuery(name = "Doctors.findByPicture", query = "SELECT d FROM Doctors d WHERE d.picture = :picture")})
public class Doctors implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "doctor_id")
    private Long doctorId;
    @Column(name = "doctor_name")
    private String doctorName;
    @Column(name = "email")
    private String email;
    @Column(name = "gender")
    private String gender;
    @Column(name = "specialty")
    private String specialty;
    @Column(name = "sub_specialty")
    private String subSpecialty;
    @Column(name = "degree")
    private String degree;
    @Column(name = "national_id")
    private String nationalId;
    @Column(name = "syndicate_number")
    private String syndicateNumber;
    @Column(name = "license_number")
    private String licenseNumber;
    @Column(name = "mobile_number")
    private String mobileNumber;
    @Column(name = "arabic_doctor_name")
    private String arabicDoctorName;
    @Column(name = "brief_profile")
    private String briefProfile;
    @Column(name = "bio")
    private String bio;
    @Column(name = "picture")
    private String picture;
    @JoinColumn(name = "doctor_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    public Doctors() {
    }

    public Doctors(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getSubSpecialty() {
        return subSpecialty;
    }

    public void setSubSpecialty(String subSpecialty) {
        this.subSpecialty = subSpecialty;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getSyndicateNumber() {
        return syndicateNumber;
    }

    public void setSyndicateNumber(String syndicateNumber) {
        this.syndicateNumber = syndicateNumber;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public String getArabicDoctorName() {
        return arabicDoctorName;
    }

    public void setArabicDoctorName(String arabicDoctorName) {
        this.arabicDoctorName = arabicDoctorName;
    }

    public String getBriefProfile() {
        return briefProfile;
    }

    public void setBriefProfile(String briefProfile) {
        this.briefProfile = briefProfile;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (doctorId != null ? doctorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Doctors)) {
            return false;
        }
        Doctors other = (Doctors) object;
        if ((this.doctorId == null && other.doctorId != null) || (this.doctorId != null && !this.doctorId.equals(other.doctorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Doctors[ doctorId=" + doctorId + " ]";
    }

}
