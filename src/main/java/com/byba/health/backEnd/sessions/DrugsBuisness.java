/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.DrugsDao;
import com.byba.health.backEnd.entities.Drugs;
import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.Visits;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class DrugsBuisness extends AbstractBusiness<Drugs> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private DrugsDao drugsDao;
    @EJB
    private PatientBuisness patientBuisness;
    @EJB
    private VisitsBuisness visitsBuisness;
    @EJB
    private FollowUpBuisness followUpBuisness;

    public DrugsBuisness() {
        super(Drugs.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Drugs> getAbstractDao() {
        if (drugsDao == null) {
            this.drugsDao = new DrugsDao(getEntityManager());
        }
        return drugsDao;
    }

    public DrugsDao getDrugsDao() {
        getAbstractDao();
        return drugsDao;
    }
    public List<Drugs> getDrugsByFollow(FollowUp followUp) throws Exception{
        return getDrugsDao().getDrugsByFollow(followUp);
    }

}
