/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.SecretaryDao;
import com.byba.health.backEnd.entities.Secretary;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class SecretaryBuisness extends AbstractBusiness<Secretary> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private SecretaryDao secretaryDao;

    public SecretaryBuisness() {
        super(Secretary.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Secretary> getAbstractDao() {
        if (secretaryDao == null) {
            this.secretaryDao = new SecretaryDao(getEntityManager());
        }
        return secretaryDao;
    }

    public SecretaryDao getAppointmentsDao() {
        getAbstractDao();
        return secretaryDao;
    }

}
