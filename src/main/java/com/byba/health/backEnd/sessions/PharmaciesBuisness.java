/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.PharmaciesDao;
import com.byba.health.backEnd.entities.Pharmacies;
import com.byba.health.middleWare.dto.PharSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class PharmaciesBuisness extends AbstractBusiness<Pharmacies> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private PharmaciesDao pharmaciesDao;

    public PharmaciesBuisness() {
        super(Pharmacies.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Pharmacies> getAbstractDao() {
        if (pharmaciesDao == null) {
            this.pharmaciesDao = new PharmaciesDao(getEntityManager());
        }
        return pharmaciesDao;
    }

    public PharmaciesDao getPharmaciesDaoDao() {
        getAbstractDao();
        return pharmaciesDao;
    }
    public Pharmacies getPharmaciesByEmail(String mail) {
        return getPharmaciesDaoDao().getPharmaciesByEmail(mail);
    }
    public List<PharSearchDto> getPharsSearch(SearchDto searchDto) {
        return getPharmaciesDaoDao().getPharsSearch(searchDto);
    }

}
