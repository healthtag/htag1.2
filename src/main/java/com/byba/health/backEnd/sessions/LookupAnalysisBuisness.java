/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.LookupAnalysisDao;
import com.byba.health.backEnd.entities.LookupAnalysis;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class LookupAnalysisBuisness extends AbstractBusiness<LookupAnalysis> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private LookupAnalysisDao lookupAnalysisDao;

    public LookupAnalysisBuisness() {
        super(LookupAnalysis.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<LookupAnalysis> getAbstractDao() {
        if (lookupAnalysisDao == null) {
            this.lookupAnalysisDao = new LookupAnalysisDao(getEntityManager());
        }
        return lookupAnalysisDao;
    }

    public LookupAnalysisDao getAppointmentsDao() {
        getAbstractDao();
        return lookupAnalysisDao;
    }

}
