/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.LapsDao;
import com.byba.health.backEnd.entities.Analysis;
import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Laps;
import com.byba.health.middleWare.dto.LapSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class LapsBuisness extends AbstractBusiness<Laps> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private LapsDao lapsDao;

    public LapsBuisness() {
        super(Laps.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Laps> getAbstractDao() {
        if (lapsDao == null) {
            this.lapsDao = new LapsDao(getEntityManager());
        }
        return lapsDao;
    }

    public LapsDao getLapsDao() {
        getAbstractDao();
        return lapsDao;
    }
    public Laps getLapsByEmail(String mail) {
        return getLapsDao().getLapsByEmail(mail);
    }
    public List<LapSearchDto> getLapsSearchData(SearchDto searchDto) {
        return getLapsDao().getLapsSearchData(searchDto);
    }


}
