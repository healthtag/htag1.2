/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.RadiologyDao;
import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.RadiationCenters;
import com.byba.health.backEnd.entities.Radiology;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class RadiologyBuisness extends AbstractBusiness<Radiology> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private RadiologyDao radiologyDao;

    public RadiologyBuisness() {
        super(Radiology.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Radiology> getAbstractDao() {
        if (radiologyDao == null) {
            this.radiologyDao = new RadiologyDao(getEntityManager());
        }
        return radiologyDao;
    }

    public RadiologyDao getRadiologyDao() {
        getAbstractDao();
        return radiologyDao;
    }

    public List<Radiology> getRadiologyByFollow(FollowUp followUp) throws Exception{
        return getRadiologyDao().getRadiologyByFollow(followUp);
    }

    public List<Radiology> getRadiologyByFollowAndStatus(FollowUp followUp, RadiationCenters radiationCenters) {
        return getRadiologyDao().getRadiologyByFollowAndStatus(followUp, radiationCenters);
    }

    public List<Radiology> getCurrentRadiologies(RadiationCenters radiationCenters) {
        List<Radiology> radiologies = getRadiologyDao().getCurrentRadiologies(radiationCenters);
        return radiologies;
    }

}
