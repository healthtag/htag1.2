/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.DoctorsDao;
import com.byba.health.backEnd.entities.Doctors;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.middleWare.dto.DoctorSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class DoctorsBuisness extends AbstractBusiness<Doctors> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private DoctorsDao doctorsDao;

    public DoctorsBuisness() {
        super(Doctors.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Doctors> getAbstractDao() {
        if (doctorsDao == null) {
            this.doctorsDao = new DoctorsDao(getEntityManager());
        }
        return doctorsDao;
    }

    public DoctorsDao getDoctorsDao() {
        getAbstractDao();
        return doctorsDao;
    }

    public Doctors getDoctorByDoctorId(UserProfile loginName) {
        return getDoctorsDao().getDoctorByLoginName(loginName);
    }
    public Doctors getDoctorByEmail(String mail) {
        return getDoctorsDao().getDoctorByMail(mail);
    }
    public Doctors getDoctorByName(String name) {
        return getDoctorsDao().getDoctorByName(name);
    }

    public List<DoctorSearchDto> getDoctorssBySearchDto(SearchDto searchDto) {
        String sql ="";
        if(searchDto.getGovernment() != null)
        {
             sql = "select c.address, c.phone1, d.doctorName, d.email, d.specialty, d.subSpecialty, d.picture from " +
                    "Clinics c , Doctors d where c.doctorId.userId = d.doctorId " +
                    "and c.governmentLocation = '"+searchDto.getGovernment()+"'";

        }else{
             sql = "select c.address, c.phone1, d.doctorName, d.email, d.specialty, d.subSpecialty, d.picture from " +
                    "Clinics c , Doctors d where c.doctorId.userId = d.doctorId ";

        }
        return (List<DoctorSearchDto>) getEntityManager().createQuery(sql).getResultList();

    }

}
