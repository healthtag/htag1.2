/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.AssistantDao;
import com.byba.health.backEnd.entities.Assistant;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class AssistantBuisness extends AbstractBusiness<Assistant> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private AssistantDao assistantDao;

    public AssistantBuisness() {
        super(Assistant.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Assistant> getAbstractDao() {
        if (assistantDao == null) {
            this.assistantDao = new AssistantDao(getEntityManager());
        }
        return assistantDao;
    }

    public AssistantDao getAppointmentsDao() {
        getAbstractDao();
        return assistantDao;
    }

}
