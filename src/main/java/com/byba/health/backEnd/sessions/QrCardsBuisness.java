/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.QrCardsDao;
import com.byba.health.backEnd.entities.Cardslookup;
import com.byba.health.backEnd.entities.QrCards;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class QrCardsBuisness extends AbstractBusiness<QrCards> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private QrCardsDao qrCardsDao;
    @EJB
    CardsLookupBuisness cardsLookupBuisness;

    public QrCardsBuisness() {
        super(QrCards.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<QrCards> getAbstractDao() {
        if (qrCardsDao == null) {
            this.qrCardsDao = new QrCardsDao(getEntityManager());
        }
        return qrCardsDao;
    }

    public QrCardsDao getQrCardsDao() {
        getAbstractDao();
        return qrCardsDao;
    }

    public QrCards getQrCodeByCardNumber(String cardNumber) {
        return getQrCardsDao().getQrCodeByCardNumber(cardNumber);
    }
    public List<QrCards> getListQrCodeByCardNumber(String cardNumber) {
        return getQrCardsDao().getListQrCodeByCardNumber(cardNumber);
    }

    public String addCard(QrCards entity) {
        entity.setIsActive(true);
        create(entity);
        Cardslookup cardslookup = new Cardslookup();
        cardslookup.setCardnumber(entity.getCardNumber());
        cardslookup.setPincode(entity.getCardPin());
        Cardslookup cardByPinAndNumber = cardsLookupBuisness.getCardByPinAndNumber(cardslookup);
        cardByPinAndNumber.setUsed(true);
        cardsLookupBuisness.edit(cardslookup);

        return entity.getCardId().toString();
    }
    public String updateCard(QrCards entity) {
        entity.setIsActive(true);
        edit(entity);
        Cardslookup cardslookup = new Cardslookup();
        cardslookup.setCardnumber(entity.getCardNumber());
        cardslookup.setPincode(entity.getCardPin());
        Cardslookup cardByPinAndNumber = cardsLookupBuisness.getCardByPinAndNumber(cardslookup);
        cardByPinAndNumber.setUsed(true);
        cardsLookupBuisness.edit(cardslookup);

        return entity.getCardId().toString();
    }

    public QrCards addNewCard() {
        QrCards qrCards = new QrCards();
        qrCards.setIsActive(false);
        qrCards.setCardNumber(null);
        qrCards.setCardPin(null);
        create(qrCards);
        return qrCards;
    }
    public void deleteQrCodeByCardNumber(String cardNum) {
        getQrCardsDao().getQrCodeByCardNumber(cardNum);
    }
}
