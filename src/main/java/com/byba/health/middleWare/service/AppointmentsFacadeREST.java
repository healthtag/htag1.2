/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Appointments;
import com.byba.health.backEnd.sessions.AppointmentsBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("appointments")
public class AppointmentsFacadeREST {

    @EJB
    AppointmentsBuisness appointmentsBuisness;

    public AppointmentsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Appointments entity) {
        appointmentsBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Appointments entity) {
        appointmentsBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        appointmentsBuisness.remove(appointmentsBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Appointments find(@PathParam("id") Integer id) {
        return appointmentsBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Appointments> findAll() {
        return appointmentsBuisness.findAll();
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(appointmentsBuisness.count());
    }

}
