/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.LookupDrugs;
import com.byba.health.backEnd.sessions.LookupDrugsBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("LookupDrugs")
public class LookupDrugsFacadeREST {

    @EJB
    LookupDrugsBuisness lookupDrugsBuisness;

    public LookupDrugsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(LookupDrugs entity) {
        lookupDrugsBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, LookupDrugs entity) {
        lookupDrugsBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        lookupDrugsBuisness.remove(lookupDrugsBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public LookupDrugs find(@PathParam("id") Integer id) {
        return lookupDrugsBuisness.find(id);
    }

    @GET
    @Produces("application/json")
    public List<LookupDrugs> findAll() {
        return lookupDrugsBuisness.findAll();
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(lookupDrugsBuisness.count());
    }

}
