/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Visits;
import com.byba.health.backEnd.sessions.VisitsBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("Visits")
public class VisitsFacadeREST {

    @EJB
    VisitsBuisness visitsBuisness;

    public VisitsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Visits entity) {
        visitsBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Visits entity) {
        visitsBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        visitsBuisness.remove(visitsBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Visits find(@PathParam("id") Integer id) {
        return visitsBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Visits> findAll() {
        return visitsBuisness.findAll();
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(visitsBuisness.count());
    }

}
