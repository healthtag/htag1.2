/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;

import com.byba.health.backEnd.entities.Attachments;
import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.backEnd.entities.Visits;
import com.byba.health.backEnd.sessions.*;
import com.byba.health.middleWare.dto.AddPatientDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.*;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("patient")
public class PatientFacadeREST {

    @EJB
    PatientBuisness patientBuisness;
    @EJB
    UserProfileBuisness userProfileBuisness;
    @EJB
    AttachmentBuisness attachmentBuisness;
    @EJB
    VisitsBuisness visitsBuisness;


    public PatientFacadeREST() {
    }

    /*
    select count(patient.card_id) from patient inner join qr_cards on patient.card_id=qr_cards.card_id where is_active = true

    * */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Patient create(Patient entity) {
        try {
            patientBuisness.create(entity);
            return entity;
        } catch (Exception e) {
            return null;
        }

    }

    @POST
    @Path("addPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public AddPatientDto addPatient(Patient entity) {
        try {
            return patientBuisness.add(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
            AddPatientDto addPatientDto = new AddPatientDto();
            addPatientDto.setStatus("2");
            return addPatientDto;
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces(MediaType.TEXT_PLAIN)
    public Patient edit(Patient entity) {
        return patientBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        patientBuisness.remove(patientBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Patient find(@PathParam("id") Integer id) {
        return patientBuisness.find(id);
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(patientBuisness.count());
    }

    @GET
    @Path("countOfCards")
    @Produces("text/plain")
    public String countOfCards() {
        return String.valueOf(patientBuisness.getCountOfCards());
    }

    @GET
    @Path("countOfCardsPerDate/{date}")
    @Produces("text/plain")
    public String countOfCardsPerDate(@PathParam("date") String date) {
        try {
            return String.valueOf(patientBuisness.getCountOfCardsPerDate(date));
        } catch (Exception e) {
            return "Invalid Request your request may be as : http://www.htag.health/bypass/patient/countOfCardsPerDate/2018-01-18";
        }
    }

    @GET
    @Path("CardsStatistics")
    @Produces("text/plain")
    public String CardsStatistics() {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        System.out.println(date);
        String s = "COUNT OF CARDS is       : " + String.valueOf(patientBuisness.getCountOfCards());
        s += "\nCOUNT OF CARDS TODAY is : " + String.valueOf(patientBuisness.getCountOfCardsPerDate(date));
        return s;


    }

    @GET
    @Path("checkName/{name}")
    @Produces("text/plain")
    public String checkName(@PathParam("name") String name) {
        Patient patient = patientBuisness.checkName(name);
        if (patient == null)
            return "true";
        else
            return "false";
    }

    @GET
    @Path("clearCard/{cardNum}")
    @Produces("text/plain")
    public void clearCard(@PathParam("cardNum") String cardNum) {
        patientBuisness.clearCard(cardNum);
    }


    @GET
    @Path("findAll")
    @Produces("application/json")
    public String findAll() {
        String jsonString = null;
        try {
            List<Patient> patients = patientBuisness.findAll();

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"patientName", "email", "password", "mobileNumber", "dateOfBirth", "idNo", "nationalId", "gender", "picture", "relativeMobileNumber", "government", "area", "address", "maritalStatus", "offspring", "chronicDiseases",
                    "presentMedications", "allergien", "surgeries", "bloodGroup", "patientId", "userId", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(patients);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @GET
    @Path("doctorPatients/{doctorId}")
    @Produces("application/json")
    public String findAllByDoctor(@PathParam("doctorId") Long doctorId) {
        String jsonString = null;
        try {
            List<Patient> patients = visitsBuisness.getPatientsByDoctors(doctorId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"patientName", "email", "password", "mobileNumber", "dateOfBirth", "idNo", "nationalId", "gender", "relativeMobileNumber", "government", "area", "address", "maritalStatus", "offspring", "chronicDiseases",
                    "presentMedications", "allergien", "surgeries", "bloodGroup", "patientId", "userId", "type", "picture", "diabetes", "hypertension", "smoker"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(patients);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @GET
    @Path("findAllAttachments/{userId}")
    @Produces("application/json")
    public String findAllAttachments(@PathParam("userId") Long userId) {
        String jsonString = null;
        try {
            UserProfile userByLoginName = userProfileBuisness.find(userId);
            List<Attachments> attachmentses = attachmentBuisness.findByUserId(userByLoginName);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "documentName", "documentType", "date", "uploadedDate", "documentPathName", "ext"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(attachmentses);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;

    }

    @GET
    @Path("findAttachment/{picName}")
    @Produces("application/json")
    public String findAllAttachments(@PathParam("picName") String picName, @Context HttpServletRequest httpRequest) {
        String jsonString = null;
        try {
            jsonString = encoder("\\uploadedFiles\\" + picName);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer();
        //    Boolean isActive = patient.getCardId().getIsActive();
        try {
            jsonString = writer.writeValueAsString(jsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonString;
    }

    public String encoder(String imagePath) {
        String base64Image = "";
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
//            String type = base64Image.split(";")[0].split("/")[1];


        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        return base64Image;
    }

    @POST
    @Path("getPatientProfileById/{id}")
    @Produces("application/json")
    public String getPatientProfileById(@PathParam("id") String id) {
        String jsonString = null;
        try {
            Patient patient = patientBuisness.getPatientById(Long.parseLong(id));

//            byte[] decodedBytes = Base64.getUrlDecoder().decode(patient.getPicture());
//            String decodedUrl = new String(decodedBytes);
//            patient.setPassword(decodedUrl);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);

            String[] Fieldnames = {"patientId", "patientName", "email", "mobileNumber", "dateOfBirth", "idNo", "picture", "nationalId", "gender",
                    "relativeMobileNumber", "government", "area", "address", "maritalStatus", "offspring", "chronicDiseases",
                    "presentMedications", "allergien", "surgeries", "bloodGroup", "userPassword", "diabetes", "hypertension", "smoker",
                    "userProfile", "userId", "firstName", "loginName", "type",
                    "cardId", "isActive", "cardNumber", "cardPin"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            //    Boolean isActive = patient.getCardId().getIsActive();
            jsonString = writer.writeValueAsString(patient);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @POST
    @Path("getLabNameByPatientId/{id}")
    @Produces("application/json")
    public String getPatientLabById(@PathParam("id") String id) {
        String labName = null;
        try {
            Patient patient = patientBuisness.getPatientById(Long.parseLong(id));
            List<Visits> visitsByPatientId = visitsBuisness.getVisitsByPatientId(patient);
            if(!visitsByPatientId.isEmpty() && visitsByPatientId != null && visitsByPatientId.size() > 0 ) {
                Visits firstVisit = visitsByPatientId.get(0);
                UserProfile userProfile = firstVisit.getClinicId().getDoctorId();
                String type = userProfile.getType();
                if (type.equals("laps") && patient.getUserProfile().getLaps() != null)
                    labName = patient.getUserProfile().getLaps().getLapName();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return labName;
    }

    @POST
    @Path("getPatientProfile/{loginName}")
    @Produces("application/json")
    public String getUserProfileByName(@PathParam("loginName") String loginName) {

        UserProfile userProfile = userProfileBuisness.getUserByLoginName(loginName);
        String jsonString = null;
        Patient patient = null;
        try {
            if (userProfile != null)
                patient = patientBuisness.getPatientById(userProfile.getPatient().getPatientId());

//            byte[] decodedBytes = Base64.getUrlDecoder().decode(patient.getPicture());
//            String decodedUrl = new String(decodedBytes);
//            patient.setPassword(decodedUrl);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);

            String[] Fieldnames = {"patientId", "patientName", "email", "password", "mobileNumber", "dateOfBirth", "idNo", "picture", "nationalId", "gender",
                    "relativeMobileNumber", "government", "area", "address", "maritalStatus", "offspring", "chronicDiseases",
                    "presentMedications", "allergien", "surgeries", "bloodGroup", "userPassword", "diabetes", "hypertension", "smoker",
                    "userProfile", "userId", "firstName", "defaultPassword", "userPassword", "loginName", "type",
                    "cardId", "isActive", "cardNumber", "cardPin"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            //    Boolean isActive = patient.getCardId().getIsActive();
            jsonString = writer.writeValueAsString(patient);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @POST
    @Path("GetPatientByCardNumber/{code}")
    @Produces("application/json")
    public String GetPatientByCardNumber(@PathParam("code") String cardNumber) {
        String jsonString = null;
        try {
            Patient patient = patientBuisness.getQrCodeByCardNumber(cardNumber);
//            byte[] decodedBytes = Base64.getUrlDecoder().decode(patient.getPicture());
//            String decodedUrl = new String(decodedBytes);
//            patient.setPassword(decodedUrl);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"patientId", "patientName", "email", "password", "mobileNumber", "dateOfBirth", "idNo", "picture", "nationalId", "gender",
                    "relativeMobileNumber", "government", "area", "address", "maritalStatus", "offspring", "chronicDiseases",
                    "presentMedications", "allergien", "surgeries", "bloodGroup"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            //    Boolean isActive = patient.getCardId().getIsActive();
            jsonString = writer.writeValueAsString(patient);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("uploadFile/{userId}/{docName}")
    public String uploadFile(
            Attachments attachment, @PathParam("userId") String userId, @PathParam("docName") String docName, @Context HttpServletRequest httpRequest) {

        try {
            attachment.setUploadedDate(new Date());
            UserProfile userProfile = new UserProfile();
            userProfile.setUserId(Long.valueOf(userId));
            attachment.setUserId(userProfile);
            String extension = getFileExtension(docName);
            List<String> extList = new ArrayList();
            extList.add("jpg");
            extList.add("pdf");
            extList.add("jpeg");
            extList.add("gif");
            extList.add("png");
            extList.add("doc");
            extList.add("xls");
            extList.add("xlsx");
            extList.add("txt");
            extList.add("ppt");
            boolean dot = docName.contains(".");
            if (dot == true) {
                int count = StringUtils.countMatches(docName, ".");
                if (count > 1) {
                    return "false";
                } else if (!extList.contains(extension.toLowerCase())) {
                    return "false";
                }

            } else {
                return "false";
            }

            int random = (int) Math.floor(Math.random() * 999);
            String filePath = httpRequest.getSession().getServletContext().getRealPath("/");
            String DocumentPathName = random + "_" + userId + "_" + new Date().getTime() + "_" + docName;
            String base64Image = attachment.getAttachment().split(",")[1];
            byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
//            try (OutputStream stream = new FileOutputStream(filePath + "\\" + "uploadedFiles\\" + DocumentPathName)) {
//                stream.write(imageBytes);
//            }
            try (OutputStream stream = new FileOutputStream("\\uploadedFiles\\" + DocumentPathName)) {
                stream.write(imageBytes);
            }
            attachment.setDocumentPathName(DocumentPathName);
            attachment.setExt(extension);
            attachmentBuisness.create(attachment);
            return "true";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";

        }

    }

    private String getFileExtension(String name) {
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

//    @POST
//    @Consumes("application/json")
//    @Path("verify/{mobileNumber}")
//    public String verify(@PathParam("mobileNumber") String mobileNumber) {
//
//
//        String customerId = "8F4F6CF7-8687-488A-A8D7-EE41DD97165C";
//        String apiKey = "aHSYi3U8uL+Mg7+nK0OKtAAK3ZGhi/xjdAYb1AeZBd2GDOxQW6k2OMT7VFl+X9Fd6+ZnRpjJAeXpZU8apvoG3w==";
//
//        String phoneNumber = mobileNumber;
//        String verifyCode = Util.randomWithNDigits(5);
//
//        String message = String.format("your verify code is  %s", verifyCode);
//        String messageType = "OTP";
//
//        try {
//
//            MessagingClient messagingClient = new MessagingClient(customerId, apiKey);
//            RestClient.TelesignResponse telesignResponse = messagingClient.message(phoneNumber, message, messageType, null);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return verifyCode;
//    }
//    @POST
//    @Consumes( "application/json")
//    @Path("verifys/{mobileNumber}")
//    public String verifys(@PathParam("mobileNumber") String mobileNumber) {
//
//        String API_KEY ="ab62195c";
//        String API_SECRET ="97024d560eeef541";
//        String verifyCode = Util.randomWithNDigits(5);
////        mobileNumber = 01069609237;
//        AuthMethod auth = new TokenAuthMethod(API_KEY, API_SECRET);
//        NexmoClient client = new NexmoClient(auth);
//        VerifyResult request = null;
//        try {
//            request = client.getVerifyClient().verify(mobileNumber, "HealthTag Code");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (NexmoClientException e) {
//            e.printStackTrace();
//        }
//
//        String requestId = request.getRequestId();
//        CheckResult result = client.check(requestId, verifyCode);
//
//
//    }

}

