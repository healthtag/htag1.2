/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Clinics;
import com.byba.health.backEnd.entities.Doctors;
import com.byba.health.backEnd.sessions.ClinicsBuisness;
import com.byba.health.backEnd.sessions.DoctorsBuisness;
import com.byba.health.backEnd.sessions.UserProfileBuisness;
import com.byba.health.middleWare.dto.DoctorSearchDto;
import com.byba.health.middleWare.dto.SearchDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("doctors")
public class DoctorsFacadeREST {

    @EJB
    DoctorsBuisness doctorsBuisness;
    @EJB
    UserProfileBuisness userProfileBuisness;
    @EJB
    ClinicsBuisness clinicsBuisness;

    public DoctorsFacadeREST() {
    }

    @POST
    @Consumes("application/json")
    public void create(Doctors entity) {
        doctorsBuisness.create(entity);
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public String edit(Doctors entity) {
        String jsonString = null;
        try {
            Doctors edit = doctorsBuisness.edit(entity);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"doctorId", "doctorName", "email", "gender", "specialty", "subSpecialty", "degree",
                    "nationalId", "syndicateNumber", "licenseNumber", "mobileNumber", "picture", "briefProfile", "bio",
                    "userProfile", "userId", "defaultPassword", "userPassword", "loginName", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(edit);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();

        }
        return jsonString;
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        doctorsBuisness.remove(doctorsBuisness.find(id));
    }

    @GET
    @Path("getDoctorProfile/{loginName}")
    @Produces("application/json")
    public String getUserProfileByName(@PathParam("loginName") String loginName) {
        String jsonString = null;
        try {
            Doctors doctor = doctorsBuisness.getDoctorByEmail(loginName);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"doctorId", "doctorName", "email", "gender", "specialty", "subSpecialty", "degree",
                    "nationalId", "syndicateNumber", "licenseNumber", "mobileNumber", "picture", "briefProfile", "bio",
                    "userProfile", "userId", "defaultPassword", "userPassword", "loginName", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(doctor);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @GET
    @Path("checkName/{name}")
    @Produces("text/plain")
    public String checkName(@PathParam("name") String name) {
        Doctors doctorByName = doctorsBuisness.getDoctorByName(name);
        if (doctorByName == null)
            return "true";
        else
            return "false";
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Doctors find(@PathParam("id") Integer id) {
        return doctorsBuisness.find(id);
    }

    @POST
    @Path("doctorSearch")
    @Produces("application/json")
    public String search(SearchDto searchDto) {
        String jsonString = null;
        try {
            List<DoctorSearchDto> all = doctorsBuisness.getDoctorssBySearchDto(searchDto);

            ObjectMapper mapper = new ObjectMapper();//address,phone1,doctor_name,email,specialty,sub_specialty
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"address", "phone", "doctorName", "email", "specialty", "subSpecialty", "picture"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(all);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @GET
    @Produces("application/json")
    public String findAll() {
        String jsonString = null;
        try {
            List<Doctors> all = doctorsBuisness.findAll();

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"doctorId", "doctorName", "email", "gender", "specialty", "subSpecialty", "degree",
                    "nationalId", "syndicateNumber", "licenseNumber", "mobileNumber", "picture", "briefProfile", "bio",
                    "userProfile", "userId", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(all);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(doctorsBuisness.count());
    }

}
