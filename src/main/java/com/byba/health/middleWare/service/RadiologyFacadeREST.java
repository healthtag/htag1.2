/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Radiology;
import com.byba.health.backEnd.sessions.RadiologyBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("Radiology")
public class RadiologyFacadeREST {

    @EJB
    RadiologyBuisness radiologyBuisness;

    public RadiologyFacadeREST() {
    }

    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Radiology entity) {
        radiologyBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Radiology entity) {
        radiologyBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        radiologyBuisness.remove(radiologyBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Radiology find(@PathParam("id") Integer id) {
        return radiologyBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Radiology> findAll() {
        return radiologyBuisness.findAll();
    }


    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(radiologyBuisness.count());
    }

}
