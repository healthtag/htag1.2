/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;

import com.byba.health.backEnd.entities.RequestDemo;
import com.byba.health.backEnd.sessions.RequestDemoBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;

/**
 * @author IbrahimShedid
 */
@Path("requestDemo")
public class RequestDemoFacadeREST {

    @EJB
    RequestDemoBuisness requestDemoBuisness;

    public RequestDemoFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(RequestDemo entity) {
        requestDemoBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, RequestDemo entity) {
        requestDemoBuisness.edit(entity);
    }



}
