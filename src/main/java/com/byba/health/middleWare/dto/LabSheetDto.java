package com.byba.health.middleWare.dto;

import com.byba.health.backEnd.entities.FollowUp;

/**
 * Created by Administrator on 11/17/2018.
 */
public class LabSheetDto {
    private FollowUp followUp;
    private Long lapId;
    private Long patientid;

    public FollowUp getFollowUp() {
        return followUp;
    }

    public void setFollowUp(FollowUp followUp) {
        this.followUp = followUp;
    }

    public Long getLapId() {
        return lapId;
    }

    public void setLapId(Long lapId) {
        this.lapId = lapId;
    }

    public Long getPatientid() {
        return patientid;
    }

    public void setPatientid(Long patientid) {
        this.patientid = patientid;
    }
}