package com.byba.health.middleWare.dto;

/**
 * Created by Administrator on 11/17/2018.
 */
public class SearchDto {
    private String specialty;
    private String area;
    private String government;

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }
}
