package com.byba.health.middleWare.dto;

import com.byba.health.backEnd.entities.Patient;

/**
 * Created by Administrator on 11/17/2018.
 */
public class AddPatientDto {
    private Patient patient;
    private String status;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
